CPU_RANGE="1 2 3 4 5 10"
NETWORK_FACTOR_RANGE="0 0.125 0.25 0.5 1 2 4"

parallel --bar -j6 python3.6 /Users/atomassi/untitled/infocom_scheduling/main_exp.py -p {1} -c {2} -n {3} ::: $(ls /Users/atomassi/untitled/infocom_scheduling/instances_100/rand/random*) ::: $CPU_RANGE ::: $NETWORK_FACTOR_RANGE
