This repository contains algorithm for the Scheduling with network constraints problem.

### Dependencies

To compile this code, you need the following dependencies:

* [Cmake](https://cmake.org/). A cross-platform family of tool for building and testing C/C++ projects.
* [boost](http://www.boost.org/). Used for graphs and multidimensioanl arrays. Note that lower version might still work too.
* [range-v3](https://github.com/ericniebler/range-v3). An experimental library for ranges algorithms in C++
* [CppRO](https://github.com/nhuin/cppro/). A miscellaneous library containing function and classes for Operational Research that I build during my PhD

While boost is a well distributed library, range-v3 and CppRo are less commonly available. If you don't want to install them on your system, refer to the following section to read how to use CMake to download them for you.

### How to build

Before being able to build the executables, you need to have all dependencies build.
CMake will take care of downloading the dependencies (except for Boost) thanks to the DownloadProject cmake script available from [here](https://github.com/Crascit/DownloadProject) by setting the SCHEDULING_DOWNLOAD_DEPENDENCIES option.

<!-- You can specify a custom installation folder for the librairies by giving the argument -DCMAKE_PREFIX_INSTALL=/path/to/install to the cmake command. -->

Once you cloned this repository, you can use the following set of command to build the SchedulingDCN executable:
```bash
cd scheduling-algorithm
git checkout download_dependencies
mkdir build && cd build
CXX=clang++ cmake .. -DSCHEDULING_DOWNLOAD_DEPENDENCIES=1
make SchedulingDCN
```

You should now find the SchedulingDCN binary in the build folder.

### Invoking the algorithms

Currently, five algorithms are implemented:

* *list*, the classic list scheduling algorithm (deprecated)
* *listDelay*, a list scheduling algorithm taking into account the delay (deprecated)
* *listScheduling*, the first version of our algorithm
* *listSchedulingMaster*, the second version of our algorithm
* *DSC*, the DSC algorithm proposed in ??

Go to the results folder and call

```bash
../build/SchedulingDCN {name of the instance} -m {name of algorithm}
```

Instances are found in the [results/instances](results/instances) folder. Their name are their basename, i.e., the file  K1_10_google_google_1501103097.inst contains the instance  K1_10_google_google_1501103097. Thus, to solve the K1_10_google_google_1501103097 instance on 3 machines with the listScheduling algorithm, use the following command line:
```bash
../build/SchedulingDCN K1_10_google_google_1501103097 -m listScheduling -c 3
```

For more informations on the command line options, use the --help option.