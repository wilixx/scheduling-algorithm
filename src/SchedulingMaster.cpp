#include "SchedulingMaster.hpp"
#include <set>

namespace Scheduling {
SchedulingMaster::SchedulingMaster(const Scheduling::Instance& _inst)
    : inst(&_inst)
    , maxTime([&]() {
        const auto edges = inst->workflow.getEdges();
        return 1 + std::accumulate(inst->times.begin(), inst->times.end(), 0ul)
               + std::accumulate(edges.begin(), edges.end(), 0ul,
                     [&](const int _acc, const Graph::Edge& __e) {
                         return inst->workflow.getEdgeWeight(__e) + _acc;
                     });
    }())
    , m_solution(*inst)
    , machineCompletion(inst->nbCPU, 0)
    , taskStarts(inst->workflow.getOrder(), -1)
    , taskAssignment(inst->workflow.getOrder(), -1)
    , minStartTime(boost::extents[inst->workflow.getOrder()][inst->nbCPU])
    , inDegrees(getInDegrees(inst->workflow))
    , longestBranches(getPrio(*inst))
    , isMaster([&]() {
        DBOUT(std::clog << "longestBranches: " << longestBranches << '\n';)
        auto startNode = std::max_element(longestBranches.begin(), longestBranches.end());
        while (inDegrees[std::distance(longestBranches.begin(), startNode)] > 0) {
            startNode = std::max_element(std::next(startNode), longestBranches.end());
        }

        std::vector<bool> retval(inst->workflow.getOrder(), false);
        Graph::Node startNodeIndex = std::distance(longestBranches.begin(), startNode);
        Graph::Path masterBranch{startNodeIndex};
        while (_inst.workflow.getOutDegree(masterBranch.back()) > 0) {
            retval[masterBranch.back()] = true;
            for (const auto v : _inst.workflow.getNeighbors(masterBranch.back())) {
                if (longestBranches[v] == *startNode) {
                    masterBranch.push_back(v);
                    continue;
                }
            }
        }
        retval[masterBranch.back()] = true;
        return retval;
    }())
    , sumOfComsToMaster([&]() {
        std::vector<int> retval(inst->workflow.getOrder(), 0);
        for (Graph::Node task1 = 0; task1 < inst->workflow.getOrder(); ++task1) {
            for (const auto& task2 : inst->workflow.getNeighbors(task1)) {
                if (isMaster[task2]) {
                    retval[task1] += inst->workflow.getEdgeWeight(task1, task2);
                }
            }
        }
        return retval;
    }())
    , exteMachinePred(boost::extents[inst->workflow.getOrder()][inst->nbCPU]) {
    std::fill(minStartTime.data(), minStartTime.data() + minStartTime.num_elements(), 0);
    DBOUT(std::clog << "sumOfComsToMaster: " << sumOfComsToMaster << '\n';)
    DBOUT(std::clog << "Master nodes:";
          for (int u = 0; u < inst->workflow.getOrder(); ++u) {
              if (isMaster[u]) {
                  std::clog << " " << u;
              }
          } std::clog
          << '\n';)
}

std::tuple<bool, int, int> SchedulingMaster::sortValue(const std::pair<int, int>& _p) {
    const auto& precedingForeignTasks = exteMachinePred[_p.first][_p.second];
    return std::make_tuple(
        !isMaster[_p.first],
        std::accumulate(precedingForeignTasks.begin(),
            precedingForeignTasks.end(), 0,
            [&](const int ___s, const int& ___p) {
                return ___s + inst->workflow.getEdgeWeight(___p, _p.first);
            }),
        longestBranches[_p.first]);
}

void SchedulingMaster::buildPairsJobMachines() {
    L.clear();

    while (L.empty()) {
        std::vector<int> waitingForZero;
        // Build list of task/machine pairs
        for (const auto& task : A) {
            for (int k = 0; k < inst->nbCPU; ++k) {
                // DBOUT(std::cout << "minStartTime" << std::make_pair(task, k) << ": " << minStartTime(task, k) << '\n';)
                if (minStartTime[task][k] <= currentT) {
                    if (k == 0) { // If master machine, no need to check for return communications
                        L.emplace_back(task, k);
                    } else if (!isMaster[task] || (isMaster[task] && inDegrees[task] == 1)) {
                        // Don't place master task on non master machine, unless it doesn't have incoming communication from non master tasks
                        if (sumOfComsToMaster[task] > 0) {
                            // If need to send back to master tasks, check it's not faster to wait for master machine to be free
                            const auto contState = m_solution.findFirstContiguousSlot(std::make_pair(k, 0), sumOfComsToMaster[task],
                                inst->times[task] + minStartTime[task][k]);
                            assert(contState.found);
                            // DBOUT(std::clog << std::make_tuple(contState.comStart, "+", sumOfComsToMaster[task], "<", inst->times[task], "+", machineCompletion[0]) << '\n';)
                            if (contState.comStart + sumOfComsToMaster[task] < inst->times[task] + machineCompletion[0]) {
                                // DBOUT(std::clog << "Not faster to wait for master machine to be free for task "<< task <<"\n");
                                L.emplace_back(task, k);
                            } else {
                                waitingForZero.push_back(task);
                                DBOUT(std::clog << "Should wait for 0 to be free to schedule " << task << "\n");
                            }
                        } else {
                            L.emplace_back(task, k);
                        }
                    }
                }
            }
        }
        if (L.empty()) {
            currentT = minStartTime[waitingForZero[0]][0];
            for (int i = 1; i < waitingForZero.size(); ++i) {
                currentT = std::min(currentT, minStartTime[waitingForZero[i]][0]);
            }
        }
    }
    // if(L.empty()) {
    //     currentT = minStartTime(waitingForZero[0], 0);
    //     for(int i = 1; i < waitingForZero.size(); ++i) {
    //         currentT = std::min(currentT, minStartTime(waitingForZero[i], 0));
    //     }

    //     DBOUT(std::clog << "currentT for 0: " << currentT << '\n';)
    //     for(const auto& task : waitingForZero) {
    //         DBOUT(std::clog << task << " -> " << minStartTime(task, 0) << '\n';)
    //         if (minStartTime(task, 0) <= currentT) {
    //             L.emplace_back(task, 0);
    //         }
    //     }
    // }
    // assert(!L.empty());

    std::sort(L.begin(), L.end(), [&](const auto& __p1, const auto& __p2) {
        return this->sortValue(__p1) < this->sortValue(__p2);
    });

    DBOUT(std::clog << "L: " << L << '\n';)
}

Solution SchedulingMaster::solve() {
    for (Graph::Node u = 0; u < inst->workflow.getOrder(); ++u) {
        if (inDegrees[u] == 0) {
            A.push_back(u);
        }
    }

    int taskScheduled = 0;
    while (!A.empty()) {
        DBOUT(std::clog << "currentT: " << currentT << '\n';)
        buildPairsJobMachines();

        for (const auto & [ v, k ] : L) {
            // Find valid routing scheduling
            std::vector<int> comStarts;
            int startTime = minStartTime[v][k];
            const auto& foreignPredecessors = exteMachinePred[v][k];

            // Master tasks don't need to place the communication
            // If no communication is needed, skip the part where you try to place the communication
            if (!foreignPredecessors.empty() && !isMaster[v]) {
                comStarts.reserve(foreignPredecessors.size());
                // Try to place every network comm greedily
                for (int i = 0; i < foreignPredecessors.size(); ++i) {
                    const auto& u = foreignPredecessors[i];

                    // Search for possible communication task between u and v
                    // space on network machine

                    const auto contState = m_solution.findFirstContiguousSlot(
                        std::make_pair(taskAssignment[u], k),
                        inst->workflow.getEdgeWeight(u, v),
                        taskStarts[u] + inst->times[u], currentT);

                    if (contState.found) {
                        startTime = std::max(startTime, contState.comStart 
                            + inst->workflow.getEdgeWeight(u, v));
                        comStarts.push_back(contState.comStart);
                        // Schedule network comm on copies
                        m_solution.scheduleCommTask({u, taskAssignment[u]},
                            {v, k}, contState.comStart);
                    } else { // No cont. slots, need to change minStartTime
                        assert(contState.nbContiguousSlotAtEnd 
                            < inst->workflow.getEdgeWeight(u, v));
                        DBOUT(std::clog << "Could not schedule (" << u 
                            << ", " << v << "): " 
                            << contState.nbContiguousSlotAtEnd << " vs. " 
                            << inst->workflow.getEdgeWeight(u, v) << '\n';)
                        minStartTime[v][k] =
                            std::max<int>(minStartTime[v][k],
                                currentT + inst->workflow.getEdgeWeight(u, v)
                                - contState.nbContiguousSlotAtEnd);
                        break;
                    }
                }
            }
            if (comStarts.size() < foreignPredecessors.size() 
                && !isMaster[v]) {
                // Revert communication tasks
                for (int i = 0; i < comStarts.size(); ++i) {
                    const int u = foreignPredecessors[i];
                    m_solution.revertScheduleCommTask({u, taskAssignment[u]},
                        {v, k}, comStarts[i]);
                }
            } else {
                ++taskScheduled;
                taskAssignment[v] = k;
                taskStarts[v] = std::max(startTime, machineCompletion[k]);
                // int lastTime = machineCompletion[k];
                machineCompletion[k] = taskStarts[v] + inst->times[v];

                DBOUT(std::clog << "Scheduling " << v << " on machine " << k << " at time " << taskStarts[v] << '\n';)
                // Place task on CPU
                for (int task = 0; task < inst->workflow.getOrder(); ++task) {
                    minStartTime[task][k] =
                        std::max(minStartTime[task][k], machineCompletion[k]);
                }
                m_solution.scheduleCPUTask(v, k, taskStarts[v]);
                A.erase(std::remove(A.begin(), A.end(), v), A.end());

                for (const auto& w : inst->workflow.getNeighbors(v)) {
                    if (--inDegrees[w] == 0) { // w can be done because all preceding are placed
                        A.push_back(w);
                    }
                    if (isMaster[w] && k != 0) {
                        // Find free network slots on machine 0
                        const auto contState = m_solution.findFirstContiguousSlot(
                            std::make_pair(k, 0), inst->workflow.getEdgeWeight(v, w), machineCompletion[k]);
                        
                        assert(contState.found);
                        // Place automatically transmission back to master machine
                        m_solution.scheduleCommTask({v, k}, {w, 0}, contState.comStart);
                        minStartTime[w][0] = std::max(minStartTime[w][0],
                            contState.comStart + inst->workflow.getEdgeWeight(v, w));
                    }
                    for (int k1 = 0; k1 < inst->nbCPU; ++k1) {
                        if (k1 == k) {
                            minStartTime[w][k] =
                                std::max(machineCompletion[k], minStartTime[w][k]);
                        } else {
                            minStartTime[w][k1] = std::max(
                                minStartTime[w][k1],
                                std::max(machineCompletion[k1],
                                    machineCompletion[taskAssignment[v]]
                                    + inst->workflow.getEdgeWeight(v, w)));
                            if (!isMaster[w]) {
                                exteMachinePred[w][k1].push_back(v);
                            }
                        }
                    }
                }
                break;
            }
        }

        int t = maxTime;
        for (const auto& task : A) {
            for (int k = 0; k < inst->nbCPU; ++k) {
                // If the task can be done on CPU k at time t
                if (minStartTime[task][k] <= t) {
                    // If CPU is the first one, no problem because no need to take into account return datas
                    if (k == 0) {
                        t = std::min(t, minStartTime[task][k]);
                        DBOUT(std::clog << "Can do " << task << " on machine " << k << " on time " << t << "\n";)
                    }
                    //else, check that the time to execute and send back the data is less than wait to execute on the first CPU
                    // First check that task is not a master task with dependencies from non master or not a master task at all
                    else if (!isMaster[task] || (isMaster[task] && inDegrees[task] == 1)) {
                        const int timeToExecuteAndGetBack = sumOfComsToMaster[task]
                                                            + m_solution.findFirstContiguousSlot(std::make_pair(k, 0), sumOfComsToMaster[task],
                                                                            std::max(machineCompletion[k], minStartTime[task][k])
                                                                                + inst->times[task])
                                                                  .comStart;
                        const int timeToExecuteOnFirstCPU = std::max(machineCompletion[0], minStartTime[task][0]) + inst->times[task];
                        if (timeToExecuteAndGetBack < timeToExecuteOnFirstCPU) {
                            t = std::min(t, minStartTime[task][k]);
                            DBOUT(std::clog << "Can do " << task << " on machine " << k << " on time " << t << "\n";)
                        }
                    }
                }
            }
        }
        // currentT should only increases ?
        currentT = t;
        std::cout << '\r' << std::setw(3) << taskScheduled << '/' << std::setw(3) << inst->workflow.getOrder() 
			<< " -> " << std::setw(10) << double(taskScheduled) / inst->workflow.getOrder() << std::flush;
        // DBOUT(std::cin.ignore();)
    }
    // std::cout << '\n';
    return m_solution;
}
} // namespace Scheduling
