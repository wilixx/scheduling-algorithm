#!/usr/bin/env bash

for TIMEOUT in 30 60 120 240 480 960 1920
do parallel --bar --timeout $TIMEOUT --resume-failed --joblog exec_wkflows_10.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "workflows_10_*") ::: $CPU_RANGE ::: $ALGOS ::: 0.5;
done
