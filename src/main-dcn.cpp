#include <iterator>

#include <MyTimer.hpp>
#include <tclap/CmdLine.hpp>

#include "DSC.hpp"
#include "ListScheduling.hpp"
#include "SchedulingMaster.hpp"
#include "utility.hpp"

struct Param {
    std::string model;
    std::string instance;
    int nbCPU;
    double networkFactor;
};

Param getParams(int argc, char** argv);
Scheduling::Solution dispatch(const Scheduling::Instance& _inst, const Param& _params);

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ',
        "1.0");

    TCLAP::ValuesConstraint<std::string> validModels(
        {"list", "listDelay", "listScheduling", "DSC", "listSchedulingMaster", "listNoCapacity"});
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
        false, "list", &validModels);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> instanceName(
        "instance", "Specify the instance name", true, "", "instance");
    cmd.add(&instanceName);

    TCLAP::ValueArg<int> nbCPU(
        "c", "nbCPU", "Specify the number of available CPU", false, 1, "int");
    cmd.add(&nbCPU);

    TCLAP::ValueArg<double> networkFactor(
        "n", "networkFactor", "Specify the network factor", false, 1, "float");
    cmd.add(&networkFactor);

    cmd.parse(argc, argv);

    return {modelName.getValue(), instanceName.getValue(), nbCPU.getValue(),
        networkFactor.getValue()};
}

int main(int argc, char** argv) {
    Param params = getParams(argc, argv);
    std::cout << "Instance name: " << params.instance << '\n';
    std::cout << "# CPU: " << params.nbCPU << '\n';
    std::cout << "Network factor: " << params.networkFactor << '\n';

    std::cout << "Building instance..." << std::flush;
    Scheduling::Instance inst = Scheduling::readFromFile("./instances/" + params.instance + ".inst", params.nbCPU);

    if(0.0 < params.networkFactor && params.networkFactor < 1.0) {std::transform(inst.times.begin(), inst.times.end(), inst.times.begin(), [&](auto&& _time){
            return _time * (1.0 / params.networkFactor);
        });
    } else {
        for (const auto& edge : inst.workflow.getEdges()) {
            inst.workflow.setEdgeWeight(edge, inst.workflow.getEdgeWeight(edge) * params.networkFactor);
        }    
    }
    
    std::cout << "done...\n";
    const std::string filename = "./results/" + params.instance + "_" + params.model + "_" + std::to_string(params.nbCPU) + "_" + std::to_string(params.networkFactor) + ".res";
    auto sol = dispatch(inst, params);
    if(0.0 < params.networkFactor && params.networkFactor < 1.0) {
        sol.makespan *= params.networkFactor;
    }
    std::cout << "Makespan: " << sol.makespan << '\n';
    sol.save(filename);
    return 0;
}

Scheduling::Solution dispatch(const Scheduling::Instance& _inst, const Param& _params) {
    if (_params.model == "list") {
        const auto sol = list(_inst);
        if (!sol.isValid()) {
            sol.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }
        return sol;
    } else if (_params.model == "listDelay") {
        const auto sol = listDelay(_inst);
        if (!sol.isValid()) {
            sol.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }
        return sol;
    } else if (_params.model == "listNoCapacity") {
        const auto sol = listNoCapacity(_inst);
        if (!sol.isValid()) {
            sol.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }
        return sol;
    } else if (_params.model == "DSC") {
        const auto sol = DSC(_inst);
        if (!sol.isValid()) {
            sol.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }
        return sol;
    } else if (_params.model == "listScheduling") {
        const auto solNormal = listScheduling(_inst);
        if (!solNormal.isValid()) {
            solNormal.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }

        std::cout << "Trying on reverse workflow\n";
        const auto reverseWorkflow = _inst.workflow.getReversedGraph();
        Scheduling::Instance instReverse(reverseWorkflow, _inst.times, _inst.nbCPU);
        const auto solReverse = listScheduling(instReverse);
        if (!solReverse.isValid()) {
            solReverse.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }
        if (solNormal.makespan < solReverse.makespan) {
            return solNormal;
        } else {
            return solReverse.reverse(_inst);
        }
    } else if (_params.model == "listSchedulingMaster") {
        const auto solNormal = Scheduling::SchedulingMaster(_inst).solve();
        //solNormal.save(filename);
        if (!solNormal.isValid()) {
            solNormal.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }

        const auto reverseWorkflow = _inst.workflow.getReversedGraph();
        Scheduling::Instance instReverse(reverseWorkflow, _inst.times, _inst.nbCPU);
    
        const auto solReverse = Scheduling::SchedulingMaster(instReverse).solve();
        //solReverse.save(filename);
        if (!solReverse.isValid()) {
            solReverse.printScheduling(std::cout);
            std::cout << "Solution is not valid!\n";
            exit(66);
        }
        if (solNormal.makespan < solReverse.makespan) {
            return solNormal;
        } else {
            return solReverse.reverse(_inst);
        }
    }
    exit(-1);
}
