import json
import argparse
import Scheduling
import collections
import os
import sqlite3
import tarfile
import time
import networkx as nx

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def save_to_json(instanceName, nbCPU, model, networkFactor):
	metrics = Scheduling.readResults("./results/{instanceName}_{model}_{nbCPU}_{networkFactor}.res".format( 
		instanceName=instanceName, model=model, nbCPU=nbCPU, networkFactor=networkFactor))

	json_filename = "./results/{instanceName}.json".format(instanceName=instanceName)
	if os.path.exists(json_filename):
		print "Loading", json_filename
		json_data = None
		with open(json_filename, 'r+') as jsonfile:
			json_data = json.load(jsonfile)
		if model in json_data:
			if nbCPU in json_data[model]:
				json_data[model][nbCPU][networkFactor] = metrics['makespan']
			else:
				json_data[model][nbCPU] = {networkFactor: metrics['makespan']}
		else:
			json_data[model] = {nbCPU: {networkFactor: metrics['makespan']}}
		print json_data
		with open(json_filename, 'w+') as jsonfile:
			json.dump(json_data, jsonfile, indent=2)
	else:
		with open(json_filename, 'w+') as jsonfile:
			json.dump({model: {nbCPU: {networkFactor: metrics['makespan']}}}, jsonfile, indent=2)
		
def init_db():
	conn = sqlite3.connect('./results.db')
	c = conn.cursor()
	c.execute('''CREATE TABLE makespan
             (instance text, algo text, nbCPU real, networkFactor real, makespan real,
             UNIQUE(instance, algo, nbCPU, networkFactor) ON CONFLICT REPLACE)''')
def save_instance_to_sql(instance, lp, sumCPU, nbNodes, density, nbTask, c):	
	try:		
		c.execute("INSERT INTO instanceInfo VALUES ('{instance}','{sumCPU}', '{lp}', '{nbNodes}', '{density}', '{nbTask}')".format(
			instance=instance, sumCPU=sumCPU, lp=lp, nbNodes=nbNodes, density=density, nbTask=nbTask))
	except Exception as e:
		print e

def save_result_to_sql(instance, nbCPU, model, networkFactor, makespan, c):
	c.execute("INSERT INTO makespan VALUES ('{instance}','{model}',{nbCPU},{networkFactor},{makespan})".format(
		instance=instance, model=model, nbCPU=nbCPU, networkFactor=networkFactor, makespan=makespan))

def show_all():
	conn = sqlite3.connect('./results.db')
	c = conn.cursor()
	workflow = "K1"
	nbNodes = "10"
	instanceName = "{workflow}_{nbNodes}".format(workflow=workflow, nbNodes=nbNodes)
	for row in c.execute("SELECT avg(makespan) FROM makespan WHERE instance LIKE '{}%'".format(instanceName)):
		print row
	for row in c.execute("SELECT avg(makespan) FROM makespan WHERE instance LIKE 'K1_120_%' AND algo LIKE 'listScheduling' AND nbCPU=4 AND networkFactor=1"):
		print row
	for row in c.execute("SELECT avg(makespan) FROM makespan WHERE instance LIKE 'K1_120_%' AND algo LIKE '2P' AND nbCPU=4 AND networkFactor=1"):
		print row

def saveResults():
        print "saveResults" 
	lastSaved = os.path.getmtime('./results.db')
	print "DB was last saved: ",  time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime(lastSaved))
	results_to_save = []
	#print "Opening tar...",
	#with tarfile.open("./results/results.tar") as tar:
	#	print "...opened!"
        #	i = 0
	#	for result in tar:
	#		if result.mtime > lastSaved:
	#			s = result.name[len("results/"):-len(".res")].split('_')
	#			print s,
	#			networkFactor = s[-1]
	#			nbCPU = s[-2]
	#			model = s[-3]
	#			instanceName = '_'.join(s[0:-3])
	#			if s[0] != '.': # Ignore weird '._K*.res' files
	#				metrics = Scheduling.readResultsFromDescriptor(tar.extractfile(result))
	#				results_to_save.append( (instanceName, nbCPU, model, networkFactor, metrics['makespan']) )
	#		i += 1
	#		print "\rLooked through", i, "files", len(results_to_save), "to save",
	#	print "...done" 

	for root, dirs, files in os.walk("./results/"):
	 	for entry in files:
	 		if entry.endswith('.res') and os.path.getmtime("./results/"+entry) > lastSaved:
	 			s = entry[0:-len(".res")].split('_')
	 			#print s
	 			networkFactor = s[-1]
	 			nbCPU = s[-2]
	 			model = s[-3]
	 			instanceName = '_'.join(s[0:-3])
				metrics = Scheduling.readResults("./results/"+entry)
				results_to_save.append( (instanceName, nbCPU, model, networkFactor, metrics['makespan']) )

	print len(results_to_save), "result(s) to save"
	conn = sqlite3.connect('./results.db')
	c = conn.cursor()
	for res in results_to_save:
		save_result_to_sql(res[0], res[1], res[2], res[3], res[4], c)
	conn.commit()

def saveInstance():
	lastSaved = os.path.getmtime('./results.db')
	instances_to_save = []
	print "Getting files..."
	for root, dirs, files in os.walk("./instances/"):
		for entry in files:
			if entry.endswith('.inst') and os.path.getmtime("./instances/"+entry) > lastSaved:
				instance_name = str(entry[0:-len(".inst")])
				instance = Scheduling.readInstance(root+entry)
				G = nx.DiGraph()
				G.add_nodes_from([str(i)+"-" for i in range(instance["nbNodes"])])
				G.add_nodes_from([str(i)+"+" for i in range(instance["nbNodes"])])
				G.add_edges_from([(str(i)+"-", str(i)+"+", {'weight':instance["CPUtimes"][i]}) for i in range(instance["nbNodes"])])
				G.add_edges_from([(str(u)+"+", str(v)+"-", {'weight': 0}) for u, v, w in instance["edges"]])
				# nx.draw(G)
				longest_path = nx.dag_longest_path_length(G, default_weight=0)
				
				instances_to_save.append( (instance_name, longest_path, 
					sum(instance["CPUtimes"]), instance["nbNodes"], 0, 0) )
				#print instances_to_save[-1]
				#print '\r', len(instances_to_save),
	print 				
	print len(instances_to_save), "instance(s) to save"
	conn = sqlite3.connect('./results.db')
	c = conn.cursor()
	for instance in instances_to_save:
		save_instance_to_sql(instance[0], instance[1], instance[2], instance[3], instance[4], instance[5], c)
	conn.commit()

def main():
	saveResults()
	saveInstance()

if __name__ == "__main__":
    main()
	
