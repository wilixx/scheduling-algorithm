#!/usr/bin/env bash
CPU_RANGE="1 2 3 5 10"
NETWORK_FACTOR_RANGE="0 0.125 0.25 0.5 1 2 3 4"
ALGOS="listScheduling listSchedulingMaster listNoCapacity"
NB_NODES_RANGE=(20 40 80 100 120 140)

# 100 nodes, varying CPU
parallel --bar --resume --joblog exec_K1_100_CPU.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "K1_100*.inst") ::: $CPU_RANGE ::: $ALGOS ::: 0.5
parallel --bar --resume --joblog exec_K1_100_netFactor.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "K1_100*.inst") ::: 4 ::: $ALGOS ::: $NETWORK_FACTOR_RANGE
for NBNODES in 20 40 80 100 120 140
	do parallel --bar --resume --joblog "exec_K1_nbNodes_${NBNODES}.log" -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "K1_${NBNODES}_*.inst" ) ::: 4 ::: $ALGOS ::: 0.5
done

parallel --bar --resume --joblog exec_K2_100_CPU.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "K2_*_100_*.inst") ::: $CPU_RANGE ::: $ALGOS ::: 0.5
parallel --bar --resume --joblog exec_K2_100_netFactor.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "K2_*_100_*.inst") ::: 4 ::: $ALGOS ::: $NETWORK_FACTOR_RANGE
for NBNODES in 20 40 80 100 120 140
	do parallel --bar --resume --joblog "exec_K2_nbNodes_${NBNODES}.log" -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "K2_*_*_${NBNODES}_*.inst" ) ::: 4 ::: $ALGOS ::: 0.5
done

parallel --bar --resume --joblog exec_rand_100_CPU.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "random_*_100_*.inst") ::: $CPU_RANGE ::: $ALGOS ::: 0.5
parallel --bar --resume --joblog exec_rand_100_netFactor.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "random_*_100_*.inst") ::: 4 ::: $ALGOS ::: $NETWORK_FACTOR_RANGE
for NBNODES in 20 40 80 100 120 140
	do parallel --bar --resume --joblog "exec_random_nbNodes_${NBNODES}.log" -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "random_*_${NBNODES}_*.inst" ) ::: 4 ::: $ALGOS ::: 0.5
done

parallel --bar --resume --joblog exec_workflows_20_CPU.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "workflows_20*") ::: $CPU_RANGE ::: $ALGOS ::: 0.5
parallel --bar --resume --joblog exec_workflows_20_netFactor.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "workflows_20*") ::: 4 ::: $ALGOS ::: $NETWORK_FACTOR_RANGE
parallel --bar --resume --joblog exec_wkflows_all.log -j8 ../build/SchedulingDCN {1/.} -c {2} -m {3} -n {4} ::: $(find ./instances -name "workflows_*") ::: 4 ::: $ALGOS ::: 0.5
