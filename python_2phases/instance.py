import networkx as nx

from definitions import *


# import pygraphviz as pgv

class Instance:
    def __init__(self, instance_id, n_cpu_machines, logger, path=None, network_factor=1):
        self.id = instance_id
        self.n_cpu_machines = n_cpu_machines

        # read network
        if not path:
            path = os.path.join(ROOT_DIR, "instances", "{}.inst".format(instance_id))

        print("instance path = {}".format(path))

        if network_factor >= 1 or network_factor == 0:

            self.divide_factor = 1

            with open(path) as f:
                nodes, edges = [int(elem) for elem in f.readline().split()]
                self.g = nx.DiGraph(directed=True)
                line_number = 0
                for line in f:
                    elem = line.split()
                    if line_number < nodes:
                        weight = float(elem[0])
                        self.g.add_node("J{}".format(line_number), weight=weight)
                        line_number += 1
                    else:
                        u, v, weight = line.split()
                        self.g.add_edge("J{}".format(u), "J{}".format(v), weight=float(weight) * network_factor)

        else:
            multiplier = 1 / float(network_factor)
            self.divide_factor = multiplier

            with open(path) as f:
                nodes, edges = [int(elem) for elem in f.readline().split()]
                self.g = nx.DiGraph(directed=True)
                line_number = 0
                for line in f:
                    elem = line.split()
                    if line_number < nodes:
                        weight = float(elem[0])
                        self.g.add_node("J{}".format(line_number), weight=weight * multiplier)
                        line_number += 1
                    else:
                        u, v, weight = line.split()
                        self.g.add_edge("J{}".format(u), "J{}".format(v), weight=float(weight))

        logger.log("g has {} nodes and {} edges".format(len(self.g.nodes()), len(self.g.edges())))

    def draw_graph(self):
        G = pgv.AGraph(directed=True)
        for u in self.g.nodes():
            G.add_node(u, label="{}\nw={}".format(u, self.g.node[u]['weight']), fontsize=8, taillabel="lsls")
        for (u, v) in self.g.edges():
            G.add_edge(u, v, size=5, label="w={}".format(self.g[u][v]['weight']), fontsize=8, fontcolor="red")
        G.layout(prog='dot')
        G.draw(os.path.join(ROOT_DIR, "plots", "{}.pdf".format(self.id)))
