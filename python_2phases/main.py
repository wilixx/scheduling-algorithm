from definitions import *
from instance import Instance
from k_partition import Partition
from logger import Logger
from schedule import Schedule

if __name__ == "__main__":

    ##################################################################
    ################# PARAMETERS #####################################
    ####################################################################
    instance_path = os.path.join(ROOT_DIR, "instances_test", "test.inst")
    max_cpu_machines = 5
    nw_factor = 1
    ###################################################################
    ###################################################################
    ###################################################################

    instance_id = instance_path.split("/")[-1][0:-5]
    logger = Logger(instance_id, max_cpu_machines, enabled=False)

    # instance
    instance = Instance(instance_id, max_cpu_machines, logger, path=instance_path, network_factor=nw_factor)
    max_cpu_machines = min(max_cpu_machines, len(instance.g.nodes()))

    # partition
    partition = Partition(instance, logger)

    n_cpu_makespan = {}
    for n_cpu in range(1, max_cpu_machines + 1):
        logger.log("\n\n\n########considering {} CPUs########".format(n_cpu).upper())
        print("considering {} CPUs".format(n_cpu))
        partitions = partition.get_partition(n_cpu)
        schedule = Schedule(instance, partitions, logger)
        n_cpu_makespan[n_cpu] = schedule.schedule_by_layer()

        n_cpu_makespan[n_cpu] = n_cpu_makespan[n_cpu] / instance.divide_factor
        print(n_cpu_makespan[n_cpu] / instance.divide_factor)

        # to export in xlsx format and show the schedule
        # schedule.output(to_excel=True)

        print("\n")

    min_cpu = min(n_cpu_makespan, key=n_cpu_makespan.get)
    print("min number of cpu = {} with a makespan of {}".format(min_cpu, n_cpu_makespan[min_cpu]))

    res_file = os.path.join(ROOT_DIR, "results",
                            "{}_2P_{}_{}.res".format(instance.id, max_cpu_machines, nw_factor))

    with open(res_file, mode="w") as f:
        f.write("{}".format(n_cpu_makespan[min_cpu]))
