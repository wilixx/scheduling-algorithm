from networkx.algorithms.community.kernighan_lin import kernighan_lin_bisection


class Partition:
    def __init__(self, instance, logger):
        self.g_direct = instance.g
        self.g = instance.g.to_undirected()
        self.n_cpu_machines = instance.n_cpu_machines
        self.partitions_costs = {i: {} for i in range(1, min(self.n_cpu_machines, len(self.g.nodes())) + 1)}
        self.logger = logger

    def recursive_partition(self, g, p):
        K = len(g.nodes()) / p
        partitions = self.recursive_cutting(g, K, [])

        if len(partitions) > p:
            while len(partitions) > p:
                partitions.sort(key=len)
                e1 = partitions.pop(0)
                e2 = partitions.pop(0)
                partitions.append(e1.union(e2))
        return partitions

    def iterative_cutting(self, g, K):
        to_be_processed = [g]
        partitions = []

        while len(to_be_processed) > 0:

            g = to_be_processed.pop()

            p1, p2 = kernighan_lin_bisection(g, weight='weight')

            if len(p1) > K:
                to_be_processed.append(self.g.subgraph(p1))
            else:
                partitions.append(p1)

            if len(p2) > K:
                to_be_processed.append(self.g.subgraph(p2))
            else:
                partitions.append(p2)

        return partitions

    def recursive_cutting(self, g, K, res=[]):
        p1, p2 = kernighan_lin_bisection(g, weight='weight')

        if len(p1) > K:
            self.recursive_cutting(self.g.subgraph(p1), K, res)
        else:
            res.append(p1)

        if len(p2) > K:
            self.recursive_cutting(self.g.subgraph(p2), K, res)
        else:
            res.append(p2)

        return res

    def get_partition_cost(self, partition, method=1):
        max_c_time = 0

        for s in partition:
            c_time = sum(self.g.node[u]["weight"] for u in s)
            max_c_time = max(max_c_time, c_time)

        node_partition = {u: ind for ind in range(len(partition)) for u in partition[ind]}

        if method == 2:
            in_weight = {partition.index(s): 0 for s in partition}
            out_weight = {partition.index(s): 0 for s in partition}

            for (u, v) in self.g_direct.edges():
                # get partition index
                p_u = node_partition[u]
                p_v = node_partition[v]
                # if they are in 2 different sets
                if p_u != p_v:
                    in_weight[p_v] += self.g_direct[u][v]['weight']
                    out_weight[p_u] += self.g_direct[u][v]['weight']
            max_communications = max(max(in_weight.values()), max(out_weight.values()))
            cost = max_c_time + max_communications
            self.logger.log(
                "the cost of the partition is {} ({} completition + {} max nw usage)\n###".format(cost, c_time,
                                                                                                  max_communications))
            return cost

        elif method == 1:
            w_cut = 0
            for (u, v) in self.g_direct.edges():
                p_u = node_partition[u]
                p_v = node_partition[v]
                if p_u != p_v:
                    w_cut += self.g_direct[u][v]['weight']

            cost = max_c_time + w_cut / len(partition)
            self.logger.log(
                "the cost of the partition is {} ({} completition + {} avg nw load)\n###".format(cost, c_time,
                                                                                                 w_cut / len(
                                                                                                     partition)))
            return cost

    def get_best_partition(self, method=1):
        # try all the values
        for n_cpu in self.partitions_costs.keys():
            self.logger.log("trying to partition using {} cpu machines...".format(n_cpu))

            partition = self.recursive_partition(self.g, n_cpu)
            self.logger.log("{}-partition found {}".format(n_cpu, partition))

            cost_partition = self.get_partition_cost(partition, method=method)

            self.partitions_costs[n_cpu] = (cost_partition, partition)

        min_elem = min(self.partitions_costs.items(), key=lambda x: x[1][0])
        best_n_cpu, best_cost, best_partition = min_elem[0], min_elem[1][0], min_elem[1][1]
        self.logger.log("best partition is {} using {} cpu with cost {}".format(best_partition, best_n_cpu, best_cost))
        assignment = {i + 1: best_partition[i] for i in range(len(best_partition))}
        self.logger.log("assignment cpu:jobs as follows {}\n\n".format(assignment))

        # return the minimum
        return best_n_cpu, assignment

    def get_partition(self, n_cpu):

        n_nodes = len(self.g.nodes())
        if n_cpu > n_nodes:
            n_cpu = n_nodes

        self.logger.log("trying to partition using {} cpu machines...".format(n_cpu))

        if n_cpu > 1:
            partition = self.recursive_partition(self.g, n_cpu)
        else:
            partition = [set(self.g.nodes())]
        self.logger.log("{}-partition found {}".format(n_cpu, partition))

        assignment = {i + 1: partition[i] for i in range(len(partition))}

        return assignment
