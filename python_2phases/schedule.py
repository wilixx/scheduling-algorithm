import unittest

import pandas as pd

from definitions import *


class Schedule:
    def __init__(self, instance, assignment, logger):
        self.instance = instance
        self.g = instance.g
        self.logger = logger
        self.job_machine = {u: cpu_machine for cpu_machine in assignment.keys() for u in assignment[cpu_machine]}
        self.n_cpu_to_use = len(assignment.keys())
        # time info
        self.cpu = {}
        self.nw_in = {}
        self.nw_out = {}
        for cpu_id in assignment.keys():
            self.cpu[cpu_id] = {0: None}
            self.nw_out[cpu_id] = {0: None}
            self.nw_in[cpu_id] = {0: None}
        # info about start-end of jobs
        self.start = {}
        self.end = {}

    def get_jobs_layers(self):
        # map job:layer
        job_layer = {}
        # map layer: list of jobs in the layer
        layer_jobs = {}

        # get layer 0 jobs
        for u in self.g.nodes():
            if not self.g.in_edges(u):
                if 0 not in layer_jobs:
                    layer_jobs[0] = [u]
                else:
                    layer_jobs[0].append(u)
                job_layer[u] = 0

        # get the others
        next_layer = 1
        while len(job_layer.keys()) != len(self.g.nodes()):
            for u in layer_jobs[next_layer - 1]:
                for (u, v) in self.g.out_edges(u):
                    if next_layer not in layer_jobs:
                        layer_jobs[next_layer] = []

                    # check if all the sources of the incoming edges have been assigned
                    all_assigned = True
                    for (i, j) in self.g.in_edges(v):
                        if i not in job_layer.keys():
                            all_assigned = False

                    if all_assigned and v not in job_layer:
                        layer_jobs[next_layer].append(v)
                        job_layer[v] = next_layer

            next_layer += 1

        self.logger.log("layer of each job: {}".format(job_layer))
        self.logger.log("jobs of each layer: {}\n\n".format(layer_jobs))
        return job_layer, layer_jobs

    def when_cpu_available(self, machine):
        """
        :param machine:
        :return: the first time slot s.t. the cpu machine is free
        """
        return max(self.cpu[machine].keys()) + 1

    def when_all_data_available(self, job):
        """
        :param job:
        :return: return the first time slot s.t. all data from incoming edges is arrived
        """
        all_data_available = 0
        for (sender, receiver) in self.g.in_edges(job):
            if self.job_machine[sender] != self.job_machine[receiver]:
                all_data_available = max(all_data_available, self.end[(sender, receiver)])
        return all_data_available + 1

    def find_first_free_cpu_slot(self, job, machine):
        """
        :param job: the considered job
        :param machine: the considered machine
        :return: the first time slot s.t. all data from incominf edges are arrived and the cpu on the machine is available
        """
        return max(self.when_all_data_available(job), self.when_cpu_available(machine))

    def find_first_free_nw_slot(self, sender_job, m_sender, m_receiver):
        """
        :param sender_job: the job that needs to send data
        :param m_sender: the machine of the sender
        :param m_receiver: the machine of the receiver
        :return: the first time slot s.t. job is over, nw_out of the sender and nw_in of the receiver are free
        """
        return max(self.end[sender_job], max(self.nw_out[m_sender]), max(self.nw_in[m_receiver])) + 1

    def assign_cpu_job(self, job, machine, initial_slot, final_slot):
        """
        :param job:
        :param machine:
        :param initial_slot:
        :param final_slot:
        :return: nothing, just assign job on a machine from initial to final slot
        """
        for i in range(initial_slot, final_slot + 1):
            self.cpu[machine][i] = "{}".format(job)
        self.start[job] = initial_slot
        self.end[job] = final_slot

    def assign_nw_job(self, sender, receiver, m_sender, m_receiver, initial_slot, final_slot):
        """
        :param sender:
        :param receiver:
        :param m_sender:
        :param m_receiver:
        :param initial_slot:
        :param final_slot:
        :return: nothing, just assign a nw job on nw_out of the sender and nw_in of the receiver
        """
        for i in range(initial_slot, final_slot + 1):
            self.nw_out[m_sender][i] = "{}->{}".format(sender, receiver)
            self.nw_in[m_receiver][i] = "{}->{}".format(sender, receiver)
        self.start[(sender, receiver)] = initial_slot
        self.end[((sender, receiver))] = final_slot

    def schedule_by_layer(self):
        map_job_layer, map_layer_jobs = self.get_jobs_layers()

        layers = len(map_layer_jobs.keys())
        current_layer = 0

        while current_layer < layers:
            self.logger.log("#######considering layer {}#######\n\n".format(current_layer))
            jobs_to_schedule = map_layer_jobs[current_layer]
            for job in jobs_to_schedule:

                machine = self.job_machine[job]

                self.logger.log("scheduling job {} assigned on machine {} ...".format(job, machine))
                self.logger.log("the job has a duration of {} time units".format(self.g.node[job]['weight']))

                initial_slot = self.find_first_free_cpu_slot(job, machine)
                final_slot = int(initial_slot + self.g.node[job]['weight']) - 1

                self.logger.log("initial time slot: {} final time slot: {}\n".format(initial_slot, final_slot))
                self.assign_cpu_job(job, machine, initial_slot, final_slot)

                if len(self.g.out_edges(job)) > 0:
                    self.logger.log("*** scheduling nw tasks from job {} and machine {} ***\n".format(job, machine))

                for (sender, receiver) in self.g.out_edges(job):

                    m_sender = self.job_machine[sender]
                    m_receiver = self.job_machine[receiver]

                    self.logger.log(
                        "scheduling edge {}->{} from machine {} to machine {}".format(sender, receiver, m_sender,
                                                                                      m_receiver))
                    self.logger.log(
                        "the job has a duration of {} time units".format(self.g[sender][receiver]['weight']))

                    if m_sender != m_receiver:
                        initial_slot = self.find_first_free_nw_slot(sender, m_sender, m_receiver)
                        final_slot = int(initial_slot + self.g[sender][receiver]['weight']) - 1
                        self.logger.log("initial time slot: {} final time slot: {}\n".format(initial_slot, final_slot))
                        self.assign_nw_job(sender, receiver, m_sender, m_receiver, initial_slot, final_slot)

                    else:
                        self.logger.log("not necessary to be scheduled, same machine")

            current_layer += 1

        self.makespan = max(self.end.values())
        self.logger.log("makespan = {}".format(self.makespan))
        # print("makespan = {}".format(self.makespan))
        self.prepare_data()
        # self.print_schedule()
        return self.makespan

    def prepare_data(self):
        for i in range(1, self.makespan + 1):
            for key in self.cpu.keys():
                if i not in self.cpu[key]:
                    self.cpu[key][i] = "X"
                if i not in self.nw_out[key]:
                    self.nw_out[key][i] = "X"
                if i not in self.nw_in[key]:
                    self.nw_in[key][i] = "X"

    def print_schedule(self):
        print("TIME\t", end="")
        print(*[_ for _ in range(1, self.makespan + 1)], sep="\t", end="\n\n")
        for key in self.cpu.keys():
            print("cpu {}:\t".format(key), end="")
            print(*[self.cpu[key][i] for i in range(1, self.makespan + 1)], sep="\t")

        for key in self.cpu.keys():
            print("\n\nin {}:\t".format(key), end="")
            print(*[self.nw_in[key][i] for i in range(1, self.makespan + 1)], sep="\t")
            print("out {}:\t".format(key), end="")
            print(*[self.nw_out[key][i] for i in range(1, self.makespan + 1)], sep="\t")

    def output(self, to_excel=False):
        res_file = os.path.join(ROOT_DIR, "results",
                                "{}_cpu{}.txt".format(self.instance.id, self.n_cpu_to_use))

        with open(res_file, mode="w") as f:
            f.write("{}".format(self.makespan))

        if not to_excel:
            return

        excel_file = os.path.join(ROOT_DIR, "output",
                                  "{}_cpu{}.xlsx".format(self.instance.id, self.n_cpu_to_use))
        csv_file = os.path.join(ROOT_DIR, "output",
                                "{}_cpu{}.csv".format(self.instance.id, self.n_cpu_to_use))

        # df = pd.DataFrame({'cpu{}'.format(i): cpu[i] for i in range(1,len(cpu.keys()))}).T
        # df = pd.DataFrame({'cpu{}'.format(i): cpu[i] for i in range(1,len(cpu.keys()))})

        df = pd.DataFrame()

        for key in self.cpu.keys():
            df["cpu{}".format(key)] = [self.cpu[key][i] for i in range(len(self.cpu[key].keys()))]
        for key in self.nw_out.keys():
            df["nw_out{}".format(key)] = [self.nw_out[key][i] for i in range(len(self.nw_out[key].keys()))]
            df["nw_in{}".format(key)] = [self.nw_in[key][i] for i in range(len(self.nw_in[key].keys()))]

        sheet_name = 'Schedule'

        # df.to_csv(csv_file)

        writer = pd.ExcelWriter(excel_file, engine='xlsxwriter')
        df.to_excel(writer, sheet_name=sheet_name)
        workbook = writer.book
        worksheet = writer.sheets[sheet_name]

        format1 = workbook.add_format({'bg_color': '#FFC7CE'})
        format2 = workbook.add_format({'bg_color': '#C6EFCE'})

        worksheet.conditional_format('A1:ZZ9999',
                                     {'type': 'text', 'criteria': 'begins with', 'value': 'X', 'format': format1})
        worksheet.conditional_format('A1:ZZ9999',
                                     {'type': 'text', 'criteria': 'begins with', 'value': 'J', 'format': format2})

        writer.save()


if __name__ == '__main__':
    unittest.main()
