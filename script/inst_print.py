#!/usr/bin/env python
'''
Module for the generation of argument for the Scheduling executables
'''

import os
import glob
import itertools
import Scheduling

def isSolved(inst, model, nbCPU, factor):
	'''
	Check if the results file for the instance exists
	'''
	filename = "./results/{}_{}_{}_{}.res".format(inst, model, nbCPU, factor)
	return os.path.isfile(filename) and os.path.getsize(filename) > 0

def printUnsolved(instanceList, cpuRange=Scheduling.CPU_RANGE,
                  models=Scheduling.MODELS,
                  nbNodesRange=Scheduling.NB_NODES_RANGE, factorRange=[1], forced=False):
	'''
	Print the instances that have not yet been solved
	'''
	for factor, nbCPU, model, nbNodes, instanceType in \
			itertools.product(factorRange, cpuRange, models, nbNodesRange, instanceList):
		instances = [v.split('/')[1][:-5] for v in \
			glob.glob("instances/{}_{}_*.inst".format(instanceType, nbNodes))]
		for inst in instances:
			if not isSolved(inst, model, nbCPU, factor) or forced: # print only instances that are not done
				print "{} -m {} -c {} -n {}".format(inst, model, nbCPU, factor)

def main():
	'''
	Main function
	'''
	printUnsolved(["K1"], nbNodesRange=Scheduling.NB_NODES_RANGE, factorRange=Scheduling.FACTOR_RANGE)
	for p in Scheduling.EDGE_PROBABILITY:
		# random
		printUnsolved(["random_{:.6f}".format(p)], nbNodesRange=Scheduling.NB_NODES_RANGE, factorRange=Scheduling.FACTOR_RANGE)
		# K2
		for pn1 in Scheduling.K2_DISTRIB:
			printUnsolved(["K2_{:.6f}_{:.6f}".format(pn1, p)], nbNodesRange=Scheduling.NB_NODES_RANGE, factorRange=Scheduling.FACTOR_RANGE)
	
	printUnsolved(["randoms", "K2s", "K1s"], nbNodesRange=range(1, 6), factorRange=Scheduling.NB_NODES_RANGE)

def printUnsolvedK1s():
	printUnsolved(["K1s"], nbNodesRange=[100, 200, 400, 800], factorRange=[1], cpuRange=[100, 200, 400, 800])

if __name__ == '__main__':
	main()

