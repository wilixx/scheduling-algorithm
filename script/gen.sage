#!/usr/bin/env sage
'''
Module for the generation of instances for the Scheduling problem
'''
import itertools
import glob
import time
import sys
import csv
import random
import collections
import argparse
import Scheduling
import pdb
import random
import os
import numpy as np

from scipy.stats import genpareto
from sage.all import DiGraph, randint, random, shuffle, choice, sample
from multiprocessing.pool import ThreadPool

def get_biggest_instances():
    workflows_sizes = []
    for root, dirs, files in os.walk("./instances/"):
        for entry in files:
            if entry.startswith("workflows"):
                try:
                    instance = Scheduling.readInstance("./instances/"+entry)
                    # print instance
                    g = Graph(instance["edges"])
                    workflows_sizes += g.connected_components_sizes()
                except:
                    pass
    print min(workflows_sizes), max(workflows_sizes)

def get_biggest_instances_v2():
    workflows_sizes = []
    for root, dirs, files in os.walk("./instances/"):
        for entry in files:
            if entry.startswith("workflowsv2_20_"):
                try:
                    instance = Scheduling.readInstance("./instances/"+entry)
                    # print instance
                    g = Graph(instance["edges"])
                    workflows_sizes += g.connected_components_sizes()
                except:
                    pass
    print min(workflows_sizes), max(workflows_sizes)

def getComCostGenerator(comCosts):
    if comCosts == "google":
        sendSize = genTaskTime()
        receiveSize = genTaskTime()
        return genTaskTime, lambda: sendSize, lambda: receiveSize
    elif comCosts == "uniform":
        return lambda: 1, lambda: 1, lambda: 1
    elif comCosts.startswith("random"):
        randRange = comCosts.split("_")
        return lambda: randint(randRange[1], randRange[2]),lambda: randint(randRange[1], randRange[2]),lambda: randint(randRange[1], randRange[2])

def readDistrib():
    '''
    Return a dictionary representing the Google distribution,
    keys are task length and values are their cardinality
    '''

    with open("{}/distrib_app.txt".format(os.path.dirname(__file__)), 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        distrib = collections.defaultdict(int)
        nbTask = 0
        for line in reader:
            distrib[int(line[1])] += 1
            nbTask += 1
        for value, total in distrib.iteritems():
            distrib[value] = total / float(nbTask)
        return distrib

def readDistribRaw():
    '''
    Return a dictionary representing the Google distribution,
    keys are task length and values are their cardinality
    '''

    with open("{}/distrib_app.txt".format(os.path.dirname(__file__)), 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        distrib = []
        nbTask = 0
        for line in reader:
            distrib.append(int(line[1]))
            nbTask += 1     
        return distrib

DISTRIB = readDistrib()

def getNbTask():
    '''
    Return a number of tasks, according to the Google distribution
    '''
    value = random()
    laskKey = None
    for key in sorted(DISTRIB.iterkeys()):
        if value > 0:
            value -= DISTRIB[key]
        else:
            break
        laskKey = key
    return laskKey

def genTaskTime():
    '''
    Return a duration for a task,  according to the Google distribution
    '''
    SIGMA = 5.4123e+06
    KHAT = 0.7681
    return max(1, int(genpareto.rvs(KHAT, 0, SIGMA) * 1e-6))

def genK1(nbTasks, taskCosts="google", comCosts="google", save=True):
    '''
    Generate a K1 with nbTasks tasks (nbTasks+2 nodes)
    '''
    print "genK1", nbTasks, taskCosts, comCosts
    depGraph = DiGraph(nbTasks)
    
    # Generate task time
    taskTimes = None
    if taskCosts == "uniform":
        taskTimes = [1 for _ in xrange(nbTasks+2)]
    elif taskCosts.startswith("random"):
        val = comCosts.split("_")
        taskTimes = [randint(val[1], val[2]) for _ in xrange(nbTasks+2)]
    elif taskCosts == "google":
        taskSize = genTaskTime()
        taskTimes = [1] + [taskSize for _ in xrange(nbTasks)] + [1]
    
    # Generate comm graph and delays
    if comCosts == "google":
        sendSize = genTaskTime()
        receiveSize = genTaskTime()
        for i in xrange(1, nbTasks+1):
            depGraph.add_edge(0, i, sendSize)
            depGraph.add_edge(i, nbTasks+1, receiveSize)
    else:
        for i in xrange(1, nbTasks+1):
            if comCosts == "uniform":
                depGraph.add_edge(0, i, 1)
                depGraph.add_edge(i, nbTasks+1, 1)
            elif comCosts.startswith("random"):
                randRange = comCosts.split("_")
                depGraph.add_edge(0, i, randint(randRange[1], randRange[2]))
                depGraph.add_edge(i, nbTasks+1, randint(randRange[1], randRange[2]))
    
    assert depGraph.is_directed_acyclic() and depGraph.is_connected()
    assert depGraph.order() == len(taskTimes)
    if save:
        filename = "./instances/K1_{}_{}_{}".format(nbTasks, taskCosts, comCosts)
        if taskCosts != "uniform" or comCosts == "uniform":
            filename += "_{}".format(getNum())
        filename += ".inst"
        saveToFile(depGraph, taskTimes, filename)
    else:
        return depGraph, taskTimes

def genK2(nbTasks, pn1=None, d=None, taskCosts="google", comCosts="google", save=True):
    '''
    \param pn1
    \param n2
    \param d
    '''
    if pn1 == None:
        pn1 = choice(Scheduling.K2_DISTRIB)
    if d == None:
        d = choice(Scheduling.EDGE_PROBABILITY)

    print "genK2", pn1, d, nbTasks, taskCosts, comCosts
    assert pn1 >= 0
    assert pn1 <= 1
    assert d >= 0
    assert d <= 1

    n1 = max(1, int(nbTasks * pn1))
    n2 = nbTasks - n1
    assert n1 >= 1
    assert n2 >= 1

    # Generate task time
    taskTimes = None
    if taskCosts == "uniform":
        taskTimes = [1 for _ in xrange(nbTasks+2)]
    elif taskCosts.startswith("random"):
        val = comCosts.split("_")
        taskTimes = [randint(val[1], val[2]) for _ in xrange(nbTasks+2)]
    elif taskCosts == "google":
        taskSize = genTaskTime()
        taskTimes = [1] + [taskSize for _ in xrange(nbTasks)] + [1]

    comCostFunc, snd_comCostFunc, rcv_comCostFunc = getComCostGenerator(comCosts)

    # Generate graph
    size = n1 + n2 + 2
    depGraph = DiGraph(size)
    s = 0
    t = size-1
    layer1Nodes = xrange(1, 1+n1)
    layer2Nodes = xrange(1+n1, 1+n1+n2)

    # Added source to layer 1 edges
    for layer1Node in layer1Nodes:
        depGraph.add_edge(s, layer1Node, snd_comCostFunc())
    # Added layer 2 nodes to sink edges
    for layer2Node in layer2Nodes:
        depGraph.add_edge(layer2Node, t, rcv_comCostFunc())

    if n1 >= n2:
        # Match each node on layer 2 with exactly one node on layer 1
        for layer1Node, layer2Node in zip(sample(layer1Nodes, n2), layer2Nodes):
            depGraph.add_edge(layer1Node, layer2Node, comCostFunc())

        # Match the remaining layer 1 nodes w/o outgoing arcs to a random layer 1 node
        for i in layer1Nodes:
            if depGraph.out_degree(i) == 0:
                depGraph.add_edge(i, choice(layer2Nodes), comCostFunc())
    else:
        # Match each node on layer 2 with exactly one node on layer 1
        for layer1Node, layer2Node in zip(layer1Nodes, sample(layer2Nodes, n1)):
            depGraph.add_edge(layer1Node, layer2Node, comCostFunc())

        # Match the remaining layer 2 nodes w/o incoming arcs to a random layer 1 node
        for i in layer2Nodes:
            if depGraph.in_degree(i) == 0:
                depGraph.add_edge(choice(layer1Nodes), i, comCostFunc())

    notConnectedNodes = [(u, v) for u, v in itertools.product(layer1Nodes, layer2Nodes) if not depGraph.has_edge(u, v)]
    nbArcs = int(((n1 * n2) - max(n1, n2)) * d)
    # assert nbArcs <= notConnectedNodes, "{} is bigger than {}".format(nbArcs, notConnectedNodes)

    for layer1Node, layer2Node in sample(notConnectedNodes, nbArcs):
        depGraph.add_edge(layer1Node, layer2Node, comCostFunc())

    assert depGraph.is_directed_acyclic() and depGraph.is_connected()
    assert depGraph.order() == len(taskTimes)
    if save:
        filename = "./instances/K2_{:.6f}_{:.6f}_{}_{}_{}".format(pn1, d, nbTasks, taskCosts, comCosts)
        if taskCosts != "uniform" or comCosts == "uniform":
            filename += "_{}".format(getNum())
        filename += ".inst"
        saveToFile(depGraph, taskTimes, filename)
    else:
        return depGraph, taskTimes

def genRandom(n, p=None, taskCosts="google", comCosts="google", save=True):
    '''
    Return a randomly generated digraph with n nodes.
    p represents the probability that an arcs exists.
    '''
    if p == None:
        p = choice(Scheduling.EDGE_PROBABILITY)

    assert p <= 1
    assert p >= 0
    assert n > 0
    print "genRandom", n, p, taskCosts, comCosts
    # Generate task time
    taskTimes = None
    # if taskCosts == "uniform":
    #   t = [1 for i in xrange(n+2)]
    # elif taskCosts.startswith("random"):
    #   val = comCosts.split("_")
    #   t = [randint(val[1], val[2]) for i in xrange(n+2)]
    # elif taskCosts == "google":
    taskTimes = [genTaskTime() for _ in xrange(n)]

    
    order = range(n)
    shuffle(order)

    depGraph = DiGraph(n)
    for i, u in enumerate(order[:-1], 1):
        v = choice(order[i:])
        assert u != v
        depGraph.add_edge(u, v, genTaskTime())

    assert depGraph.is_connected()

    # Generate comm graph and delays
    # for u, v in itertools.combinations(order, 2):
    #   if random() < p and not depGraph.has_edge(u, v):
    #       depGraph.add_edge(u, v, genTaskTime())
    arcs = [(u, v) for u, v in itertools.combinations(order, 2)]
    notConnectedNodes = [(u, v) for u, v in arcs if not depGraph.has_edge(u, v)]
    for u, v in sample(notConnectedNodes, int(len(notConnectedNodes)*p)):
        depGraph.add_edge(u, v, genTaskTime())

    assert depGraph.is_directed_acyclic()
    assert depGraph.is_connected()
    assert depGraph.order() == len(taskTimes)
    if save:
        filename = "./instances/random_{:.6f}_{}_{}_{}".format(p, n, taskCosts, comCosts)
        if taskCosts != "uniform" or comCosts == "uniform":
            filename += "_{}".format(getNum())
        filename += ".inst"
        saveToFile(depGraph, taskTimes, filename)
    else:
        return depGraph, taskTimes

def genRandoms(nbApps, taskCosts="google", comCosts="google"):
    '''
    Generate nbApps random dependency graphs. The size of the graphs is given by the function getNbTask
    '''
    print "genRandoms", nbApps, taskCosts, comCosts
    nbTasks = 0
    depGraph = DiGraph()
    taskTimes = []

    for _ in xrange(nbApps):
        n = getNbTask()
        d, t = genRandom(n, taskCosts=taskCosts, comCosts=comCosts, save=False) if n != 1 \
            else (DiGraph(1), [genTaskTime()])
        
        taskTimes += t
        depGraph.add_vertices([u+nbTasks for u in d.vertices()])
        for u, v, label in d.edge_iterator():
            depGraph.add_edge(u+nbTasks, v+nbTasks, label)
        nbTasks += d.order()
    
    assert depGraph.order() == len(taskTimes)
    
    filename = "./instances/randoms_{}_{}_{}".format(nbApps, taskCosts, comCosts)
    if taskCosts != "uniform" or comCosts == "uniform":
        filename += "_{}".format(getNum())
    filename += ".inst"
    saveToFile(depGraph, taskTimes, filename)

def genK1s(nbApps, taskCosts="google", comCosts="google"):
    '''
    Generate nbApps K1 dependency graphs. The size of the graphs is given by the function getNbTask
    '''
    print "genK1s", nbApps, taskCosts, comCosts
    nbTasks = 0
    depGraph = DiGraph()
    taskTimes = []

    for _ in xrange(nbApps):
        n = getNbTask()
        d, t = genK1(n, taskCosts=taskCosts, comCosts=comCosts, save=False) if n != 1 \
            else (DiGraph(1), [genTaskTime()])

        taskTimes += t
        depGraph.add_vertices([u+nbTasks for u in d.vertices()])
        for u, v, label in d.edge_iterator():
            depGraph.add_edge(u+nbTasks, v+nbTasks, label)
        nbTasks += d.order()
    
    assert depGraph.order() == len(taskTimes)
    
    filename = "./instances/K1s_{}_{}_{}".format(nbApps, taskCosts, comCosts)
    if taskCosts != "uniform" or comCosts == "uniform":
        filename += "_{}".format(getNum())
    filename += ".inst"
    saveToFile(depGraph, taskTimes, filename)

def genK2s(nbApps, taskCosts="google", comCosts="google"):
    '''
    Generate nbApps K2 dependency graphs. The size of the graphs is given by the function getNbTask
    '''
    print "genK2s", nbApps, taskCosts, comCosts
    nbTasks = 0
    depGraph = DiGraph()
    taskTimes = []

    for _ in xrange(nbApps):
        n = getNbTask()
        d, t = genK2(n, taskCosts=taskCosts, comCosts=comCosts, save=False) if n != 1 \
            else (DiGraph(1), [genTaskTime()])

        taskTimes += t
        depGraph.add_vertices([u+nbTasks for u in d.vertices()])
        for u, v, label in d.edge_iterator():
            depGraph.add_edge(u+nbTasks, v+nbTasks, label)
        nbTasks += d.order()
    
    assert depGraph.order() == len(taskTimes)
    filename = "./instances/K2s_{}_{}_{}".format(nbApps, taskCosts, comCosts)
    if taskCosts != "uniform" or comCosts == "uniform":
        filename += "_{}".format(getNum())
    filename += ".inst"
    saveToFile(depGraph, taskTimes, filename)

def genWorkflows(nbApps, maxSize, taskCosts="google", comCosts="google"):
    '''
    Generate nbApps K2 dependency graphs. The size of the graphs is given by the function getNbTask
    '''
    print "genWorkflows", nbApps, taskCosts, comCosts
    
    nbTask = None
    while True:
        sizeTasks = [getNbTask() for _ in xrange(nbApps)]
        if sum(sizeTasks) <= maxSize:
            break
        else:
            print("Failed to created workflow below 1000")
    depGraph = DiGraph()
    taskTimes = []
    nbTasks = 0
    for n in sizeTasks:
        workflow_type = np.random.choice([genRandom, genK1, genK2], p=[.4, .4, .2])
        d, t = workflow_type(n, taskCosts=taskCosts, comCosts=comCosts, save=False) if n != 1 \
                            else (DiGraph(1), [genTaskTime()])

        taskTimes += t
        depGraph.add_vertices([u+nbTasks for u in d.vertices()])
        for u, v, label in d.edge_iterator():
            depGraph.add_edge(u+nbTasks, v+nbTasks, label)
        assert depGraph.order() == len(taskTimes)
        nbTasks += d.order()

    # return depGraph, taskTimes
    filename = "./instances/workflows_{}_{}_{}".format(nbApps, taskCosts, comCosts)
    if taskCosts != "uniform" or comCosts == "uniform":
        filename += "_{}".format(getNum())
    filename += ".inst"
    saveToFile(depGraph, taskTimes, filename)

def genWorkflowsv2(nbApps, taskCosts="google", comCosts="google"):
    '''
    Generate nbApps K2 dependency graphs. The size of the graphs is given by the function getNbTask
    '''
    print "genWorkflows", nbApps, taskCosts, comCosts
    nbTasks = 0
    depGraph = DiGraph()
    taskTimes = []

    for _ in xrange(20):
        n = nbApps
        workflow_type = np.random.choice([genRandom, genK1, genK2], p=[.4, .4, .2])
        d, t = workflow_type(n, taskCosts=taskCosts, comCosts=comCosts, save=False) if n != 1 \
            else (DiGraph(1), [genTaskTime()])

        taskTimes += t
        depGraph.add_vertices([u+nbTasks for u in d.vertices()])
        for u, v, label in d.edge_iterator():
            depGraph.add_edge(u+nbTasks, v+nbTasks, label)
        nbTasks += d.order()
    
    assert depGraph.order() == len(taskTimes)
    # return depGraph, taskTimes
    filename = "./instances/workflowsv2_{}_{}_{}".format(nbApps, taskCosts, comCosts)
    if taskCosts != "uniform" or comCosts == "uniform":
        filename += "_{}".format(getNum())
    filename += ".inst"
    saveToFile(depGraph, taskTimes, filename)

def saveToFile(depGraph, taskTimes, filename):
    '''
    Save the dependency graph depGraph and the duration of the tasks taskTimes
    in the file named filename
    '''
    with open(filename, 'wb') as csvfile:
        w = csv.writer(csvfile, delimiter='\t')
        w.writerow([depGraph.order(), depGraph.size()])
        for time in taskTimes:
            w.writerow([time])
        for u, v, d in depGraph.edges():
            w.writerow([u, v, d])
    print "Saved to", filename

def numGen(num):
    while True:
        num += 1
        yield num

timeGen = numGen(int(time.time()))

def getNum():
    return next(timeGen)

def main():
    '''
    Launch generators for K1, random and other dependency graphs
    '''
    parser = argparse.ArgumentParser(description='Instance generator for the Scheduling problem')
    parser.add_argument('instanceType', help='name of the instance type')
    parser.add_argument('n', type=int, help='number of tasks')
    parser.add_argument('--nbWorkflow', type=int, help='number of workflows (used for allK1s, allK2s, allrands and workflows)')
    parser.add_argument('--maxSize', type=int, help='maximum size of the CPU sum (used for allK1s, allK2s, allrands and workflows)')
    parser.add_argument('-pn1', type=float, help='% tasks at layer 1 for K2')
    parser.add_argument('-p', type=float, help='edge probability for the random/K2 dependency graphs')
    parser.add_argument('--taskCosts', dest='taskCosts', default='google',
                     help='cost of the tasks \
                     (default: randomly chosen according to the google distribution')
    parser.add_argument('--comCosts', dest='comCosts', default='google',
                     help='cost of the communication \
                     (default: randomly chosen according to the google distribution')
    args = parser.parse_args()
    if args.instanceType in ["allrands", "allK1s", "allK2s", "workflows"]:
        if not args.nbWorkflow:
            parser.error("--nbWorkflow required for allrands, allK1s, allK2s and workflows instance type")
        if not args.maxSize:
            parser.error("--maxSize required for allrands, allK1s, allK2s and workflows instance type")

    pool = ThreadPool()
    if args.instanceType == "K1":
        # Generate a K1 dependency graph
        genK1(args.n, args.taskCosts, args.comCosts)
    elif args.instanceType == "random":
        # Generate a random dependency graph
        if not args.p:
            parser.error("-p required for the 'random' instance type")
        genRandom(args.n, args.p, args.taskCosts, args.comCosts)
    elif args.instanceType == "K2":
        # Generate a random dependency graph
        if not args.p:
            parser.error("-p required for the 'K2' instance type")
        if not args.pn1:
            parser.error("-pn1 required for the 'K2' instance type")

        genK2(args.n, args.pn1, args.p, args.taskCosts, args.comCosts)
    elif args.instanceType == "randoms":
        # Generate random dependency graphs
        if not args.p:
            parser.error("-p required for the 'randoms' instance type")
        genRandoms(args.n, args.taskCosts, args.comCosts)
    elif args.instanceType == "allK1":
        # Generate instances of K1 until n.args instances exists
        params = []
        for nbNodes in Scheduling.NB_NODES_RANGE:
            files = glob.glob("./instances/K1_{}_{}_{}_*.inst".format(nbNodes, args.taskCosts, args.comCosts))
            for _ in xrange(args.n - len(files)):
                params.append((nbNodes, args.taskCosts, args.comCosts))

        pool.map(lambda args: genK1(*args), params)
    elif args.instanceType == "allrand":
        # Generate instances of random until n.args instances exists
        params = []
        for nbNodes, p in itertools.product(Scheduling.NB_NODES_RANGE, Scheduling.EDGE_PROBABILITY):
            prefix = "./instances/random_{:.6f}_{}_{}_{}".format(p, nbNodes, args.taskCosts, args.comCosts)
            files = glob.glob("{}_*.inst".format(prefix))
            for _ in xrange(args.n - len(files)):
                params.append((nbNodes, p, args.taskCosts, args.comCosts))

        # pool.
        pool.map(lambda args: genRandom(*args), params)

    elif args.instanceType == "allK2":
        # Generate instances of K2 until n.args instances exists
        params = []
        for nbNodes, p, pn1 in itertools.product(Scheduling.NB_NODES_RANGE, Scheduling.EDGE_PROBABILITY, Scheduling.K2_DISTRIB):
            prefix = "./instances/K2_{:.6f}_{:.6f}_{}_{}_{}".format(pn1, p, nbNodes, args.taskCosts, args.comCosts)
            files = glob.glob("{}_*.inst".format(prefix))
            for _ in xrange(args.n - len(files)):
                params.append((nbNodes, pn1, p, args.taskCosts, args.comCosts))

        pool.map(lambda args: genK2(*args), params)
    elif args.instanceType == "allrands":
        # Generate instances of randoms until n.args instances exists
        params = []
        
        prefix = "./instances/randoms_{}_{}_{}".format(args.nbWorkflow, args.taskCosts, args.comCosts)
        files = glob.glob("{}_*.inst".format(prefix))
        for _ in xrange(args.n - len(files)):
            params.append((args.nbWorkflow, args.taskCosts, args.comCosts))
        pool.map(lambda args: genRandoms(*args), params)

    elif args.instanceType == "allK1s":
        # Generate instances of K1s until n.args instances exists
        params = []
        files = glob.glob("./instances/K1s_{}_{}_{}_*.inst".format(args.nbWorkflow, args.taskCosts, args.comCosts))
        for _ in xrange(args.n - len(files)):
            params.append((args.nbWorkflow, args.taskCosts, args.comCosts))
        pool.map(lambda args: genK1s(*args), params)
    
    elif args.instanceType == "allK2s":
        # Generate instances of K2s until n.args instances exists
        params = []
        files = glob.glob("./instances/K2s_{}_{}_{}_*.inst".format(args.nbWorkflow, args.taskCosts, args.comCosts))
        for _ in xrange(args.n - len(files)):
            params.append((args.nbWorkflow, args.taskCosts, args.comCosts))
        pool.map(lambda args: genK2s(*args), params)

    elif args.instanceType == "workflows":
        # Generate random workflows (random, K1, K2) until n.args instances exists      
        params = []
        
        files = glob.glob("./instances/workflows_{}_{}_{}_*.inst".format(args.nbWorkflow, args.taskCosts, args.comCosts))
        for _ in xrange(args.n - len(files)):
            params.append((args.nbWorkflow, args.maxSize, args.taskCosts, args.comCosts))
        pool.map(lambda args: genWorkflows(*args), params)

    elif args.instanceType == "workflowsv2":
        # Generate random workflows (random, K1, K2) until n.args instances exists      
        params = []
        
        files = glob.glob("./instances/workflowsv2_{}_{}_{}_*.inst".format(args.nbWorkflow, args.taskCosts, args.comCosts))
        for _ in xrange(args.n - len(files)):
            params.append((args.nbWorkflow, args.taskCosts, args.comCosts))
        pool.map(lambda args: genWorkflowsv2(*args), params)

    else:
        print "No workflow specified..."

if __name__ == '__main__':
    main()
