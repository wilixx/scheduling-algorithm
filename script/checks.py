import glob
import Scheduling
import inst_print
import itertools

def check_DSC_is_worst_than_ours():	
	for instance, nbCPU, netFactor in itertools.product(Scheduling.getAvailableInstances(), Scheduling.CPU_RANGE, Scheduling.FACTOR_RANGE):
		try:
			DSC_makespan = Scheduling.readResults("results/{}_DSC_{}_{}.res".format(instance, nbCPU, netFactor))["makespan"]
			ours_makespan = Scheduling.readResults("results/{}_listScheduling_{}_{}.res".format(instance, nbCPU, netFactor))["makespan"]
			master_makespan = Scheduling.readResults("results/{}_listSchedulingMaster_{}_{}.res".format(instance, nbCPU, netFactor))["makespan"]

			if DSC_makespan < ours_makespan and DSC_makespan < master_makespan: 
				print instance, nbCPU, netFactor
		except:
			pass

def check_network_factor_for_1CPU():	
	for instance, model in itertools.product(Scheduling.getAvailableInstances(), Scheduling.MODELS):
		try:
			makespans = [ Scheduling.readResults("results/{instance}_{model}_1_{netFact}.res".format(instance=instance, model=model, netFact=network_factor))["makespan"] \
				for network_factor in Scheduling.FACTOR_RANGE ]
			if len(set(makespans)) > 1:
				print instance, model, set(makespans)
		except:
			pass

def check_listSchedulingMaster_does_not_increases_with_nbCPU():
	for instance, model in itertools.product(Scheduling.getAvailableInstances(), ["listScheduling", "listSchedulingMaster"]):
		try:
			makespans = [ Scheduling.readResults("results/{instance}_{model}_{nbCPU}_{netFact}.res".format(
				instance=instance, model=model, nbCPU=nbCPU, netFact=1))["makespan"] \
				for nbCPU in Scheduling.CPU_RANGE ]
			for i in range(len(makespans)-1):
				if makespans[i] < makespans[i+1]:
					print instance, model, i
		except:
			pass

def check_listSchedulingMaster_does_not_decreases_with_netFact():
	for instance, model, nbCPU in itertools.product(Scheduling.getAvailableInstances(), ["listScheduling", "listSchedulingMaster"], Scheduling.CPU_RANGE):
		try:
			makespans = [ Scheduling.readResults("results/{instance}_{model}_{nbCPU}_{netFact}.res".format(
				instance=instance, model=model, nbCPU=nbCPU, netFact=1))["makespan"] \
				for netFactor in  Scheduling.FACTOR_RANGE]
			for i in range(len(makespans)-1):
				if makespans[i] > makespans[i+1]:
					print instance, model, i
		except:
			pass