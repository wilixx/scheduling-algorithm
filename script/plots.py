#!/usr/bin/env sage
'''
Module for plotting figures about the experiment on scheduling
'''
import glob
import csv
import math
import matplotlib.pyplot as plt
import matplotlib
import Scheduling
from multiprocessing import Pool
import sqlite3
import os

# from sage.all import *
# from multiprocessing.pool import ThreadPool

LINESTYLES = {
	"list": "--",
	"listNoCapacity": "--",
	"listDelay": "-.",
	"listScheduling": "-",
	"listSchedulingMaster": "--",
	"DSC": "--",
	"2P": "-."
}

MARKERS = {
	"list": "x",
	"listNoCapacity": "x",
	"listDelay": "+",
	"listScheduling": (5, 2),
	"listSchedulingMaster": ".",
	"DSC": (2, 3),
	"2P": (5, 2)
}

LABELS = {
	"list": "list",
	"listNoCapacity": "ListScheduler",
	"listDelay": "listDelay",
	"listScheduling": "G-List",
	"listSchedulingMaster": "G-List-Master",
	"DSC": "DSC",
	"2P": "Partition"
}

COLORS = {
	"list": "black",
	"listNoCapacity": "black",
	"listDelay": "black",
	"listScheduling": "blue",
	"listSchedulingMaster": "green",
	"DSC": "black",
	"2P": "red"
}
 

def get_ratio_machine(conn, workflow="K1", nbNodes='100', netFactor="0.5", models=Scheduling.MODELS, filename=None):
	xVals = {}
	yVals = {}
	c = conn.cursor()
	
	modelList = ", ".join(['"{}"'.format(model) for model in models])
	# Get instances done by 2P
	subRequest = None
	instanceName = "{workflow}_{nbNodes}".format(workflow=workflow, nbNodes=nbNodes)
	if instanceName != "workflows_20_%":
		subRequest = "SELECT DISTINCT instanceInfo.instance FROM makespan INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance WHERE makespan.instance LIKE '{}' AND networkFactor={} AND algo LIKE ('2P')".format(instanceName, netFactor)
	else:
		subRequest = "'workflows_20_google_google_1531950079', 'workflows_20_google_google_1531950080', 'workflows_20_google_google_1532023072', 'workflows_20_google_google_1532023073', 'workflows_20_google_google_1532023074', 'workflows_20_google_google_1532023075', 'workflows_20_google_google_1532553962', 'workflows_20_google_google_1532553964'"

	request = "SELECT algo, nbCPU, avg(makespan/max(lp, (sumCPU/nbCPU))) FROM makespan INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance WHERE makespan.instance IN ({}) AND networkFactor={} AND algo IN ({}) GROUP BY algo, nbCPU".format(subRequest, netFactor, modelList)
	values = {m:{} for m in models}
	for row in c.execute(request):
		values[row[0]][int(row[1])] = row[2]
		assert row[2] >= 1

	for model in models:
		xVals[model] = []
		yVals[model] = []
		for x, y in sorted(values[model].items()):
			xVals[model].append(x)
			yVals[model].append(y)

	return xVals, yVals

def get(conn, info, group, instanceName, setParams, models):
	yVals = {}
	xVals = {}
	c = conn.cursor()
	modelList = ", ".join(['"{}"'.format(model) for model in models])
	request = "SELECT algo, {group1}, {info} FROM makespan INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance WHERE makespan.instance {} {} AND algo IN ({}) GROUP BY algo, {group2}".format(instanceName, setParams, modelList, info=info, group1=group[0], group2=group[1])
	print(request)
	values = {m:{} for m in models}
	try:
		res = c.execute(request)
		for row in res:
			values[row[0]][float(row[1])] = row[2]
	except Exception as e:
		print(e)

	for model in models:
		xVals[model] = []
		yVals[model] = []
		for x, y in sorted(values[model].items()):
			xVals[model].append(x)
			yVals[model].append(y)
	return xVals, yVals

def get_makespan_machine(conn, workflow, nbNodes, netFactor, models):
	yVals = {}
	xVals = {}
	c = conn.cursor()
	instanceName = "{workflow}_{nbNodes}_%".format(workflow=workflow, nbNodes=nbNodes)
	modelList = ", ".join(['"{}"'.format(model) for model in models])
	request = "SELECT algo, nbCPU, avg(makespan) FROM makespan\
		INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
		WHERE makespan.instance LIKE '{}' AND networkFactor={} AND algo IN ({})\
		GROUP BY algo, nbCPU".format(instanceName, netFactor, modelList)
	print( request)
	values = {m:{} for m in models}
	for row in c.execute(request):
		values[row[0]][int(row[1])] = row[2]

	for model in models:
		xVals[model] = []
		yVals[model] = []
		for x, y in sorted(values[model].items()):
			xVals[model].append(x)
			yVals[model].append(y)
	return xVals, yVals

def latexify(fig_width=None, fig_height=None, columns=1, fontsize=15):
	'''
	Set parameters for matplotlib style to obtain IEEE figures
	'''
	assert columns in [1, 2]
	fig_widths = [3.39, 6.9]
	if fig_width is None:
	   fig_width = fig_widths[columns-1] # width in inches9

	if fig_height is None:
	   fig_height = fig_width*((math.sqrt(5)-1.0)/2.0) # height in inches

	MAX_HEIGHT_INCHES = 8.0
	if fig_height > MAX_HEIGHT_INCHES:
		print("WARNING: fig_height too large:" + fig_height +
        "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
		fig_height = MAX_HEIGHT_INCHES

	params = {'backend': 'pdf',
           # 'text.latex.preamble': ['\usepackage{gensymb}'],
           'axes.labelsize': fontsize, # fontsize for x and y LABELS (was fontsize)
           'axes.titlesize': fontsize,
           'font.size': fontsize, # was fontsize
           'legend.fontsize': fontsize, # was fontsize
           'xtick.labelsize': fontsize,
           'ytick.labelsize': fontsize,
           # 'text.usetex': True,
           'figure.figsize': [fig_width, fig_height],
           'font.family': 'serif',
	}
	matplotlib.rcParams.update(params)

def plot_ratio_size(workflow, log=False, nbNodesRange=Scheduling.NB_NODES_RANGE, netFactor='0.5', nbCPU='4', 
	models=Scheduling.MODELS, filename=None, xticks=Scheduling.NB_NODES_RANGE):
	'''
	Plot the makespan as a function of the size
	'''
	if filename == None:
		filename = "figures/makespan_size_{}_{}.pdf".format(workflow, nbCPU)
	print( "plot_ratio_size(", workflow, nbNodesRange, nbCPU, models, ")", end=' ')
	plt.clf()
	conn = sqlite3.connect('./results/results.db')
	c = conn.cursor()
	plotted = False
	for model in models:
		xVals = []
		yVals = []
		for nbNodes in nbNodesRange:
			instanceName = "{workflow}_{nbNodes}".format(workflow=workflow, nbNodes=nbNodes)
			res = c.execute("SELECT avg(makespan/max(lp, (sumCPU/nbCPU))) FROM makespan\
				INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
				WHERE makespan.instance LIKE '{}%' AND algo LIKE '{}' AND nbCPU={} AND networkFactor={}".
				format(instanceName, model, nbCPU, netFactor))
			if res.rowcount == -1:
				val = res.fetchone()
				if val[0] == None:
					continue
				xVals.append(nbNodes)
				yVals.append(float(val[0]))
			else:
				print( "No value experiment found for results/{}_{}*_{}_{}_{}.res".format(workflow, nbNodes, 
					model, nbCPU, netFactor))
		if xVals:
			print( model, xVals, yVals)
			plotted = True
			plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
				marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
			# print( model, [(u, v) for u, v in zip(xVals, yVals)])
	if plotted:
		# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		if log:
			plt.yscale("log")
			plt.ylim(bottom=1, top=100)
		else:
			plt.ylim(bottom=0, top=100)
		plt.xticks(xticks)
		plt.ylabel("Ratio")
		plt.xlabel("# tasks")
		plt.show()
		plt.savefig(filename, bbox_inches='tight')

def plot_ratio_density(workflow, log=False, nbNodes="100", nbCPU="4", netFactor="0.5", models=Scheduling.MODELS,
	filename=None):
	'''
	'''
	if filename == None:
		filename = "figures/makespan_density_{}_{}_{}.pdf".format(workflow, nbNodes, nbCPU)
	print( "plot_ratio_density(", workflow, nbNodes, models, ")")
	plt.clf()
	conn = sqlite3.connect('./results/results.db')
	c = conn.cursor()



	plotted = False
	for model in models:
		xVals = []
		yVals = []
		for density in Scheduling.EDGE_PROBABILITY:
			instanceName = "{workflow}_{density:.6f}_{nbNodes}_google_google".format(workflow=workflow,
				nbNodes=nbNodes, density=density)

			res = c.execute("SELECT avg(makespan/max(lp, (sumCPU/nbCPU))) FROM makespan\
				INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
				WHERE makespan.instance LIKE '{}%' AND algo LIKE '{}' AND nbCPU={} AND networkFactor={}".
				format(instanceName, model, nbCPU, netFactor))
			
			if res.rowcount == -1:
				val = res.fetchone()
				if val[0] == None:
					continue
				yVals.append(float(val[0]))
				xVals.append(density)
			else:
				print( "No value experiment found for results/{}_{:.6f}_{}_google_google_*_{}_*_{}.res".format(
					workflow, density, nbNodes, model, netFactor))
		if xVals:
			plotted = True
			plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
				marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
			# print( model, [(u, v) for u, v in zip(xVals, yVals)])
	if plotted:
		# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		if log:
			plt.yscale("log")
			plt.ylim(bottom=1, top=100)
		else:
			plt.ylim(bottom=0, top=100)
		plt.ylabel("Ratio")
		plt.xlabel("Edge density")
		plt.show()
		plt.savefig(filename, bbox_inches='tight')

def plot_ratio_factor(workflow="K1", log=False, nbNodes='100', nbCPU='4',
	models=Scheduling.MODELS, filename=None):
	'''
	Plot the makespan as a function of the number of CPUs available
	'''
	if filename == None:
		filename = "figures/makespan_factor_{}_{}_{}.pdf".format(workflow, nbNodes, nbCPU)
	print( "plot_ratio_factor(", workflow, nbNodes, nbCPU, models, ")")
	plt.clf()
	conn = sqlite3.connect('./results/results.db')
	c = conn.cursor()
	plotted = False
	# for model in models:
	# 	xVals = []
	# 	yVals = []
	# 	for netFactor in Scheduling.FACTOR_RANGE:
	# 		# files = glob.glob("results/{}_*_{}_{}_{}.res".format(workflow, model, nbCPU, netFactor))
	# 		# value = []
	# 		# for nameFiles in files:
	# 		# 	value.append(Scheduling.readResults(nameFiles)["makespan"])
	# 		# if value:
	# 		instanceName = "{workflow}_{nbNodes}".format(workflow=workflow, nbNodes=nbNodes)
	# 		res = c.execute("SELECT avg(makespan/max(lp, (sumCPU/nbCPU))) FROM makespan\
	# 			INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
	# 			WHERE makespan.instance LIKE '{}%' AND algo LIKE '{}' AND nbCPU={} AND networkFactor={}".
	# 			format(instanceName, model, nbCPU, netFactor))
			
	# 		if res.rowcount == -1:
	# 			val = res.fetchone()
	# 			if val[0] == None:
	# 				continue
	# 			yVals.append(float(val[0]))
	# 			assert yVals[-1] >= 1
	# 			xVals.append(netFactor)
	# 			# yVals.append(sum(value) / len(value))
	# 		else:
	# 			print( "No value experiment found for results/{}_*_{}_{}_{}.res".format(workflow, model, nbCPU,
	# 				netFactor))
	# 	if xVals:
	# 		plotted = True
	# 		plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
	# 			marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
	# 		# print( model, [(u, v) for u, v in zip(xVals, yVals)])
	instanceName = "{workflow}_{nbNodes}_%".format(workflow=workflow, nbNodes=nbNodes)

	modelList = ", ".join(['"{}"'.format(model) for model in models])
	# Get instances done by 2P
	subRequest = None
	if instanceName != "workflows_20_%":
		subRequest = "SELECT DISTINCT instanceInfo.instance FROM makespan INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance WHERE makespan.instance LIKE '{}' AND nbCPU={} AND algo LIKE ('2P')".format(instanceName, nbCPU)
	else:
		subRequest = "'workflows_20_google_google_1531950079', 'workflows_20_google_google_1531950080', 'workflows_20_google_google_1532023072', 'workflows_20_google_google_1532023073', '', 'workflows_20_google_google_1532023075', 'workflows_20_google_google_1532553962', 'workflows_20_google_google_1532553964'"

	request = "SELECT algo, networkFactor, avg(makespan/max(lp, (sumCPU/nbCPU))) FROM makespan INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance WHERE makespan.instance IN ({}) AND nbCPU={} AND algo IN ({}) GROUP BY algo, networkFactor".format(subRequest, nbCPU, modelList)
	print( request)
	values = {m:{} for m in models}
	for row in c.execute(request):
		values[row[0]][int(row[1])] = row[2]
		assert row[2] >= 1

	for model in models:
		xVals = []
		yVals = []
		print( model, values[model])
		for x, y in sorted(values[model].items()):
			xVals.append(x)
			yVals.append(y)

		if xVals:
			plotted = True
			plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
				marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
	if plotted:
		# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		if log:
			plt.yscale("log")
			plt.ylim(bottom=1, top=120)
		else:
			plt.ylim(bottom=0, top=120)
		plt.ylabel("Ratio")
		plt.xlabel("Network factor")
		plt.show()
		plt.savefig(filename, bbox_inches='tight')


###############################################################################
#
###############################################################################
def plot_makespan_size(workflow, log=False, nbNodesRange=Scheduling.NB_NODES_RANGE, netFactor='0.5', nbCPU='4', 
	models=Scheduling.MODELS, filename=None, xticks=Scheduling.NB_NODES_RANGE):
	'''
	Plot the makespan as a function of the size
	'''
	if filename == None:
		filename = "figures/makespan_size_{}_{}.pdf".format(workflow, nbCPU)
	print( "plot_makespan_size(", workflow, nbNodesRange, nbCPU, models, ")",)
	plt.clf()
	conn = sqlite3.connect('./results/results.db')
	c = conn.cursor()
	plotted = False
	for model in models:
		xVals = []
		yVals = []
		for nbNodes in nbNodesRange:
			instanceName = "{workflow}_{nbNodes}".format(workflow=workflow, nbNodes=nbNodes)
			res = c.execute("SELECT avg(makespan) FROM makespan\
				INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
				WHERE makespan.instance LIKE '{}%' AND algo LIKE '{}' AND nbCPU={} AND networkFactor={}".
				format(instanceName, model, nbCPU, netFactor))
			if res.rowcount == -1:
				val = res.fetchone()
				if val[0] == None:
					continue
				xVals.append(nbNodes)
				yVals.append(float(val[0]))
			else:
				print( "No value experiment found for results/{}_{}*_{}_{}_{}.res".format(workflow, nbNodes, 
					model, nbCPU, netFactor))
		if xVals:
			print( model, xVals, yVals)
			plotted = True
			plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
				marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
			# print( model, [(u, v) for u, v in zip(xVals, yVals)])
	if plotted:
		# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		if log:
			plt.yscale("log")
			plt.ylim(bottom=1, top=100)
		else:
			plt.ylim(bottom=0, top=100)
		plt.xticks(xticks)
		plt.ylabel("Makespan")
		plt.xlabel("# tasks")
		plt.show()
		plt.savefig(filename, bbox_inches='tight')

def plot_makespan_density(workflow, log=False, nbNodes="100", nbCPU="4", netFactor="0.5", models=Scheduling.MODELS,
	filename=None):
	'''
	'''
	if filename == None:
		filename = "figures/makespan_density_{}_{}_{}.pdf".format(workflow, nbNodes, nbCPU)
	print( "plot_makespan_density(", workflow, nbNodes, models, ")")
	plt.clf()
	conn = sqlite3.connect('./results/results.db')
	c = conn.cursor()



	plotted = False
	for model in models:
		xVals = []
		yVals = []
		for density in Scheduling.EDGE_PROBABILITY:
			instanceName = "{workflow}_{density:.6f}_{nbNodes}_google_google".format(workflow=workflow,
				nbNodes=nbNodes, density=density)

			res = c.execute("SELECT avg(makespan) FROM makespan\
				INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
				WHERE makespan.instance LIKE '{}%' AND algo LIKE '{}' AND nbCPU={} AND networkFactor={}".
				format(instanceName, model, nbCPU, netFactor))
			
			if res.rowcount == -1:
				val = res.fetchone()
				if val[0] == None:
					continue
				yVals.append(float(val[0]))
				xVals.append(density)
			else:
				print( "No value experiment found for results/{}_{:.6f}_{}_google_google_*_{}_*_{}.res".format(
					workflow, density, nbNodes, model, netFactor))
		if xVals:
			plotted = True
			plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
				marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
			# print( model, [(u, v) for u, v in zip(xVals, yVals)])
	if plotted:
		# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		if log:
			plt.yscale("log")
			plt.ylim(bottom=1, top=100)
		else:
			plt.ylim(bottom=0, top=100)
		plt.ylabel("Makespan")
		plt.xlabel("Edge density")
		plt.show()
		plt.savefig(filename, bbox_inches='tight')
			
def plot_makespan_machine(workflow="K1", log=False, nbNodes='100', netFactor="0.5", models=Scheduling.MODELS, filename=None):
	'''
	Plot the makespan as a function of the number of CPUs available
	'''

	if filename == None:
		filename = "figures/makespan_nbCPU_{}_{}.pdf".format(workflow, nbNodes)
	print( "plot_makespan_machine(worflow=", workflow, "nbNodes=", nbNodes, "models=", models, ")")
	plt.clf()
	conn = sqlite3.connect('./results/results.db')
	c = conn.cursor()
	plotted = False
	instanceName = "{workflow}_{nbNodes}_%".format(workflow=workflow, nbNodes=nbNodes)

	modelList = ", ".join(['"{}"'.format(model) for model in models])
	request = "SELECT algo, nbCPU, avg(makespan) FROM makespan\
		INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
		WHERE makespan.instance LIKE '{}' AND networkFactor={} AND algo IN ({})\
		GROUP BY algo, nbCPU".format(instanceName, netFactor, modelList)
	print( request)
	values = {m:{} for m in models}
	for row in c.execute(request):
		values[row[0]][int(row[1])] = row[2]

	for model in models:
		xVals = []
		yVals = []
		print( model, values[model])
		for x, y in sorted(values[model].items()):
			xVals.append(x)
			yVals.append(y)

		if xVals:
			plotted = True
			plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
				marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
			# print( model, [(u, v) for u, v in zip(xVals, yVals)])
	if plotted:
		# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		if log:
			plt.yscale("log")
			plt.ylim(bottom=1, top=75)
		else:
			plt.ylim(bottom=0, top=75)
		plt.xticks([1, 5, 10])
		plt.ylabel("Makespan")
		plt.xlabel("# machines")
		plt.show()
		plt.savefig(filename, bbox_inches='tight')

def plot_makespan_factor(workflow="K1", log=False, nbNodes='100', nbCPU='4',
	models=Scheduling.MODELS, filename=None):
	'''
	Plot the makespan as a function of the number of CPUs available
	'''
	if filename == None:
		filename = "figures/makespan_factor_{}_{}_{}.pdf".format(workflow, nbNodes, nbCPU)
	print( "plot_makespan_factor(", workflow, nbNodes, nbCPU, models, ")")
	plt.clf()
	conn = sqlite3.connect('./results/results.db')
	c = conn.cursor()
	plotted = False
	for model in models:
		xVals = []
		yVals = []
		for netFactor in Scheduling.FACTOR_RANGE:
			# files = glob.glob("results/{}_*_{}_{}_{}.res".format(workflow, model, nbCPU, netFactor))
			# value = []
			# for nameFiles in files:
			# 	value.append(Scheduling.readResults(nameFiles)["makespan"])
			# if value:
			instanceName = "{workflow}_{nbNodes}".format(workflow=workflow, nbNodes=nbNodes)
			res = c.execute("SELECT avg(makespan) FROM makespan\
				INNER JOIN instanceInfo ON instanceInfo.instance=makespan.instance\
				WHERE makespan.instance LIKE '{}%' AND algo LIKE '{}' AND nbCPU={} AND networkFactor={}".
				format(instanceName, model, nbCPU, netFactor))
			
			if res.rowcount == -1:
				val = res.fetchone()
				if val[0] == None:
					continue
				yVals.append(float(val[0]))
				xVals.append(netFactor)
				# yVals.append(sum(value) / len(value))
			else:
				print( "No value experiment found for results/{}_*_{}_{}_{}.res".format(workflow, model, nbCPU, netFactor))
		if xVals:
			plotted = True
			plt.plot(xVals, yVals, color=COLORS.get(model, "black"),
			marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
			# print( model, [(u, v) for u, v in zip(xVals, yVals)])
	if plotted:
		# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		if log:
			plt.yscale("log")
			plt.ylim(bottom=1)
		else:
			plt.ylim(bottom=0)
		plt.ylabel("Makespan")
		plt.xlabel("Network factor")
		plt.show()
		plt.savefig(filename, bbox_inches='tight')


def expand_makespan_size(param): 
	plot_makespan_size(**param)

def expand_makespan_density(param): 
	plot_makespan_density(**param)

def expand_makespan_machine(param): 
	plot_makespan_machine(**param)

def expand_makespan_factor(param): 
	plot_makespan_factor(**param)


def expand_ratio_size(param): 
	plot_ratio_size(**param)

def expand_ratio_density(param): 
	plot_ratio_density(**param)

def expand_ratio_machine(param): 
	plot_ratio_machine(**param)

def expand_ratio_factor(param): 
	plot_ratio_factor(**param)

def plot_legend():	
	fig = plt.figure()
	fig_legend = plt.figure()
	ax = fig.add_subplot(111)
	lines = None
	for model in Scheduling.MODELS:
		# plt.plot([], [], color=COLORS.get(model, "black"),
				# marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
		l = ax.plot([], [], color=COLORS.get(model, "black"),
				marker=MARKERS[model], linestyle=LINESTYLES[model], label=LABELS[model])
		if lines:
			lines += l
		else:
			lines = l
	fig_legend.legend(lines, [LABELS[model] for model in Scheduling.MODELS], loc='center', ncol=4)
	#fig.show()

	# figlegend.show()
	fig_legend.savefig('./figures/legend.pdf', bbox_inches='tight')

def main():
	'''
	Plots all figures
	'''
	latexify()

	pool = Pool(6)
	params_makespan = []
	params_size = []
	params_density = []
	params_factor = []

	for p in Scheduling.EDGE_PROBABILITY:
		for workflow in ["random_{:.6f}".format(p)] + ["K2_%_{:.6f}".format(p) for p in Scheduling.EDGE_PROBABILITY]:
			params_makespan.append({"workflow": workflow})
		for pn1 in Scheduling.K2_DISTRIB:
			for workflow in ["K2_{:.6f}_{:.6f}".format(pn1, p)]:
				params_makespan.append({"workflow": workflow})

		for workflow in ["K1"]:
			params_makespan.append({"workflow": workflow})
		
		# Plot makespan vs factor and size
		
		for i in Scheduling.CPU_RANGE:
			for workflow in ["K1"] + \
				["K2_{:.6f}_{:.6f}".format(pn1, p) for p in Scheduling.EDGE_PROBABILITY for pn1 in Scheduling.K2_DISTRIB] + \
				["K2_%_{:.6f}".format(p) for p in Scheduling.EDGE_PROBABILITY] + \
				["random_{:.6f}".format(p) for p in Scheduling.EDGE_PROBABILITY]:
				params_size.append({"workflow": workflow, "nbCPU": i})
		
		# for i in Scheduling.CPU_RANGE:
		# 	for workflow in ["K2_%"] + ["K2_{:.6f}".format(pn1) for pn1 in Scheduling.K2_DISTRIB] + ["random"]:
		# 		params_density.append({"workflow": workflow, "nbCPU": i})
		
		# for workflow in ["K1s"] + ["randoms_{:.6f}".format(p) for p in Scheduling.EDGE_PROBABILITY]:
		# 	params_size.append({"workflow": workflow, "nbNodesRange": xrange(1, 6), "nbCPU": i})
		
		# for workflow in ["K1s"] + ["randoms_{:.6f}".format(p) for p in Scheduling.EDGE_PROBABILITY]:
			# params_makespan.append({"nbNodes": 5, "workflow": workflow})	

	for i in Scheduling.CPU_RANGE:
		for workflow in ["K1"] + \
			["K2_{:.6f}_{:.6f}".format(pn1, p) for p in Scheduling.EDGE_PROBABILITY for pn1 in Scheduling.K2_DISTRIB] + \
			["K2_%_{:.6f}".format(p) for p in Scheduling.EDGE_PROBABILITY] + \
			["random_{:.6f}".format(p) for p in Scheduling.EDGE_PROBABILITY]:
			params_factor.append({"workflow": workflow, "nbCPU": i})

	
	print( len(params_makespan) + len(params_size) + len(params_density) + len(params_factor), " figures to plot.")
	map(expand_ratio_machine, params_ratio_machine)
	map(expand_ratio_size, params_ratio_size)
	map(expand_ratio_density, params_ratio_density)
	map(expand_ratio_factor, params_ratio_factor)

	map(expand_makespan_machine, params_makespan_machine)
	map(expand_makespan_size, params_makespan_size)
	map(expand_makespan_density, params_makespan_density)
	map(expand_makespan_factor, params_makespan_factor)

def plots_for_networking():
	'''
	Create the plot for IFIP paper
	'''
	latexify()

	pool = Pool(1)
	params_ratio_machine= []
	params_ratio_size = []
	params_ratio_density = []
	params_ratio_factor = []
	params_makespan_machine= []
	params_makespan_size = []
	params_makespan_density = []
	params_makespan_factor = []

	for logVal, logName in [(True, "log"), (False, "lin")]:
		# Density
		params_ratio_machine.append({"workflow":"K1", "log":logVal, "nbNodes":'100',
			"netFactor":"0.5", "filename":"figures/{}/ratio_nbCPU_K1.pdf".format(logName)})
		params_makespan_machine.append({"workflow":"K1", "log":logVal, "nbNodes":'100',
			"netFactor":"0.5", "filename":"figures/{}/makespan_nbCPU_K1.pdf".format(logName)})
		params_ratio_machine.append({"workflow":"K2_%_%", "log":logVal, "nbNodes":'100',
			"netFactor":"0.5", "filename":"figures/{}/ratio_nbCPU_K2.pdf".format(logName)})
		params_makespan_machine.append({"workflow":"K2_%_%", "log":logVal, "nbNodes":'100',
			"netFactor":"0.5", "filename":"figures/{}/makespan_nbCPU_K2.pdf".format(logName)})
		params_ratio_machine.append({"workflow":"random_%", "log":logVal, "nbNodes":'100',
			"netFactor":"0.5", "filename":"figures/{}/ratio_nbCPU_random.pdf".format(logName)})
		params_makespan_machine.append({"workflow":"random_%", "log":logVal, "nbNodes":'100',
			"netFactor":"0.5", "filename":"figures/{}/makespan_nbCPU_random.pdf".format(logName)})

		# # # # Network factor
		params_ratio_factor.append({"workflow":"K1", "log":logVal, "nbNodes":'100', "nbCPU":"4",
			"filename":"figures/{}/ratio_netFactor_K1.pdf".format(logName)})
		params_makespan_factor.append({"workflow":"K1", "log":logVal, "nbNodes":'100', "nbCPU":"4",
			"filename":"figures/{}/makespan_netFactor_K1.pdf".format(logName)})
		params_ratio_factor.append({"workflow":"K2_%_%", "log":logVal, "nbNodes":'100', "nbCPU":"4",
			"filename":"figures/{}/ratio_netFactor_K2.pdf".format(logName)})
		params_makespan_factor.append({"workflow":"K2_%_%", "log":logVal, "nbNodes":'100', "nbCPU":"4",
			"filename":"figures/{}/makespan_netFactor_K2.pdf".format(logName)})
		params_ratio_factor.append({"workflow":"random_%", "log":logVal, "nbNodes":'100', "nbCPU":"4",
			"filename":"figures/{}/ratio_netFactor_rand.pdf".format(logName)})
		params_makespan_factor.append({"workflow":"random_%", "log":logVal, "nbNodes":'100', "nbCPU":"4",
			"filename":"figures/{}/makespan_netFactor_rand.pdf".format(logName)})

		# # tasks
		params_ratio_size.append({"workflow":"K1", "log":logVal, "netFactor":"0.5", "nbCPU":"4",
			"xticks": [20, 80, 140], "filename":"figures/{}/ratio_nbTasks_K1.pdf".format(logName)})
		params_makespan_size.append({"workflow":"K1", "log":logVal, "netFactor":"0.5", "nbCPU":"4",
			"xticks": [20, 80, 140], "filename":"figures/{}/makespan_nbTasks_K1.pdf".format(logName)})
		params_ratio_size.append({"workflow":"K2_%_%", "log":logVal, "netFactor":"0.5", "nbCPU":"4",
			"xticks": [20, 80, 140], "filename":"figures/{}/ratio_nbTasks_K2.pdf".format(logName)})
		params_makespan_size.append({"workflow":"K2_%_%", "log":logVal, "netFactor":"0.5", "nbCPU":"4",
			"xticks": [20, 80, 140], "filename":"figures/{}/makespan_nbTasks_K2.pdf".format(logName)})
		params_ratio_size.append({"workflow":"random_%", "log":logVal, "netFactor":"0.5", "nbCPU":"4",
			"xticks": [20, 80, 140], "filename":"figures/{}/ratio_nbTasks_random.pdf".format(logName)})
		params_makespan_size.append({"workflow":"random_%", "log":logVal, "netFactor":"0.5", "nbCPU":"4",
			"xticks": [20, 80, 140], "filename":"figures/{}/makespan_nbTasks_random.pdf".format(logName)})

		# Density (K2 & rand)
		params_ratio_density.append({"workflow":"K2_%", "log":logVal, "nbNodes":'100', "netFactor":"0.5",
			"nbCPU":"4", "filename":"figures/{}/ratio_density_K2.pdf".format(logName)})
		params_makespan_density.append({"workflow":"K2_%", "log":logVal, "nbNodes":'100', "netFactor":"0.5",
			"nbCPU":"4", "filename":"figures/{}/makespan_density_K2.pdf".format(logName)})
		params_ratio_density.append({"workflow":"random", "log":logVal, "nbNodes":'100', "netFactor":"0.5",
			"nbCPU":"4", "filename":"figures/{}/ratio_density_random.pdf".format(logName)})
		params_makespan_density.append({"workflow":"random", "log":logVal, "nbNodes":'100', "netFactor":"0.5",
			"nbCPU":"4", "filename":"figures/{}/makespan_density_random.pdf".format(logName)})

		params_ratio_machine.append({"workflow":"workflows", "log":logVal, "nbNodes":'20',
			"netFactor":"0.5", "filename":"figures/{}/ratio_nbCPU_workflows.pdf".format(logName)})
		params_makespan_machine.append({"workflow":"workflows", "log":logVal, "nbNodes":'20',
			"netFactor":"0.5", "filename":"figures/{}/makespan_nbCPU_workflows.pdf".format(logName)})
		params_ratio_factor.append({"workflow":"workflows", "log":logVal, "nbNodes":'20',
			"nbCPU":"4", "filename":"figures/{}/ratio_netFactor_workflows.pdf".format(logName)})
		params_makespan_factor.append({"workflow":"workflows", "log":logVal, "nbNodes":'20',
			"nbCPU":"4", "filename":"figures/{}/makespan_netFactor_workflows.pdf".format(logName)})
		params_ratio_size.append({"workflow":"workflows", "log":logVal, "netFactor":"0.5",
			"nbNodesRange":[10, 20, 50, 100], "nbCPU":"4", "xticks": [10, 50, 100],
			"filename":"figures/{}/ratio_nbTasks_workflows.pdf".format(logName)})
		params_makespan_size.append({"workflow":"workflows", "log":logVal, "netFactor":"0.5",
			"nbNodesRange":[10, 20, 50, 100], "nbCPU":"4", "xticks": [10, 50, 100],
			"filename":"figures/{}/makespan_nbTasks_workflows.pdf".format(logName)})

		params_ratio_machine.append({"workflow":"workflowsv2", "log":logVal, "nbNodes":'20',
			"netFactor":"0.5", "filename":"figures/{}/ratio_nbCPU_workflowsv2.pdf".format(logName)})
		params_makespan_machine.append({"workflow":"workflowsv2", "log":logVal, "nbNodes":'20',
			"netFactor":"0.5", "filename":"figures/{}/makespan_nbCPU_workflowsv2.pdf".format(logName)})
		params_ratio_factor.append({"workflow":"workflowsv2", "log":logVal, "nbNodes":'20', "nbCPU":"4",
			"filename":"figures/{}/ratio_netFactor_workflowsv2.pdf".format(logName)})
		params_makespan_factor.append({"workflow":"workflowsv2", "log":logVal, "nbNodes":'20', "nbCPU":"4",
			"filename":"figures/{}/makespan_netFactor_workflowsv2.pdf".format(logName)})
		params_ratio_size.append({"workflow":"workflowsv2", "log":logVal, "netFactor":"0.5",
			"nbNodesRange":[10, 20, 50, 100], "nbCPU":"4",
			"filename":"figures/{}/ratio_nbTasks_workflowsv2.pdf".format(logName)})
		params_makespan_size.append({"workflow":"workflowsv2", "log":logVal, "netFactor":"0.5",
			"nbNodesRange":[10, 20, 50, 100], "nbCPU":"4",
			"filename":"figures/{}/makespan_nbTasks_workflowsv2.pdf".format(logName)})

	# plot_legend()
	pool.map(expand_ratio_machine, params_ratio_machine)
	# pool.map(expand_ratio_size, params_ratio_size)
	# pool.map(expand_ratio_density, params_ratio_density)
	# pool.map(expand_ratio_factor, params_ratio_factor)

	# pool.map(expand_makespan_machine, params_makespan_machine)
	# pool.map(expand_makespan_size, params_makespan_size)
	# pool.map(expand_makespan_density, params_makespan_density)
	# pool.map(expand_makespan_factor, params_makespan_factor)
