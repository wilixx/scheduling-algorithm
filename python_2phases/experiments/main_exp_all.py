import argparse

from definitions import *
from instance import Instance
from k_partition import Partition
from logger import Logger
from schedule import Schedule

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('-p', '--path', dest='path', type=str, action='store', required=True,
                        help='path with the instances')
    parser.add_argument('-c', '--cpu', dest='cpu', type=int, action='store', required=True,
                        help='max number of cpu machines')
    parser.add_argument('-n', '--network_factor', dest='network_factor', type=float, action='store', required=True,
                        help='network_factor')

    args = parser.parse_args()
    path = args.path
    max_cpu_machines = args.cpu
    network_factor = args.network_factor
    print("****************")
    print(path)
    print("****************")

    instance_id = path.split("/")[-1][0:-5]
    logger = Logger(instance_id, max_cpu_machines, network_factor, enabled=False)
    instance = Instance(instance_id, max_cpu_machines, logger, path=path, network_factor=network_factor)

    # instance.draw_graph()

    partition = Partition(instance, logger)

    max_cpu = min(max_cpu_machines, len(instance.g.nodes()))
    n_cpu_makespan = {}
    for n_cpu in range(1, max_cpu + 1):
        logger.log("\n\n\n########considering {} CPUs########".format(n_cpu).upper())
        print("considering {} CPUs".format(n_cpu))
        partitions = partition.get_partition(n_cpu)
        schedule = Schedule(instance, partitions, logger)
        n_cpu_makespan[n_cpu] = schedule.schedule_by_layer()

        n_cpu_makespan[n_cpu] = n_cpu_makespan[n_cpu] / instance.divide_factor
        print(n_cpu_makespan[n_cpu] / instance.divide_factor)
        # schedule.output(to_excel=True)

        print("\n")

    min_cpu = min(n_cpu_makespan, key=n_cpu_makespan.get)
    print("min number of cpu = {} with a makespan of {}".format(min_cpu, n_cpu_makespan[min_cpu]))

    res_file = os.path.join(ROOT_DIR, "results",
                            "{}_2P_{}_{}.res".format(instance.id, max_cpu_machines, network_factor))

    with open(res_file, mode="w") as f:
        f.write("{}".format(n_cpu_makespan[min_cpu]))
